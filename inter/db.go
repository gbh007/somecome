// Название пакета специальное, ибо инит нельзя, значит мы поинтим XD
package inter

import (
	"context"
	"fmt"
	"somecome/pkg/errs"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	Host     string
	Port     int
	User     string
	Password string
	DBName   string
}

func NewStorage(ctx context.Context, cfg Config) (*sqlx.DB, error) {
	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DBName,
	)

	db, err := sqlx.Open("postgres", psqlInfo)
	if err != nil {
		return nil, errs.WrapError(ctx, nil, err)
	}

	return db, nil
}
