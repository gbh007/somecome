FROM amd64/alpine:3.17

# добавление ssl сертификатов и пакета временых зон
RUN apk update && apk add ca-certificates tzdata

RUN mkdir /app

COPY main /app/main

RUN chmod +x /app/main

WORKDIR /app
EXPOSE 8080
VOLUME /app/files

ENTRYPOINT ["/app/main"]