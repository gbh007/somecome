build:
	go build -o main

run: build
	./main -c .hidden/config.json

dev: build
	./main --developer -c .hidden/config.json

docker:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build --trimpath -o main
	docker tag somecome:latest somecome:old || true
	docker build -f Dockerfile -t somecome:latest .
	docker stop somecome || true
	docker rm somecome || true
	docker run --restart always -d --name somecome -p 17080:8080 -v ./.hidden/files:/app/files -v ./.hidden/config-docker.json:/app/config.json  somecome:latest
	docker rmi somecome:old || true
