package superobject

import (
	"context"
	"io"
	txtpDTO "somecome/domain/txtp/dto"
)

// TextProcessorDomain - домен обработки текстов
type TextProcessorDomain interface {
	// Parse - разбирает поток на текстовые блоки, согласно указанному диалекту
	Parse(ctx context.Context, r io.Reader, dialect txtpDTO.Dialect) ([]*txtpDTO.TextUnit, error)
}
