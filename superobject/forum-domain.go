package superobject

import (
	"context"
	forumDTO "somecome/domain/forum/dto"
)

type ForumDomain interface {
	Forum(ctx context.Context, id int64) (*forumDTO.Forum, error)
	Forums(ctx context.Context) ([]*forumDTO.Forum, error)
	NewForum(ctx context.Context, userID int64, forumName string) (int64, error)
}
