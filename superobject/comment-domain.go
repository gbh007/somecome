package superobject

import (
	"context"
	commentDTO "somecome/domain/comment/dto"
)

type CommentDomain interface {
	MessageCount(ctx context.Context, contentKey string) (int64, int64, error)
	Messages(ctx context.Context, contentKey string, page *int64) ([]*commentDTO.Message, error)
	NewMessage(ctx context.Context, message *commentDTO.MessageInput) (int64, error)
	// Message - возвращает сообщение на контент, по его ID
	Message(ctx context.Context, id int64, includeAttachments bool) (*commentDTO.Message, error)
	// UpdateMessageText - обновляет текст сообщения, по его ID
	UpdateMessageText(ctx context.Context, userID, messageID int64, text string, dialect *string) error
}
