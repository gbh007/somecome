package superobject

import (
	"context"
	"net/http"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/link"
)

type WebServerDomain interface {
	// ParseJSON - парсит тело запроса в переданный объект
	ParseJSON(r *http.Request, data interface{}) error

	NoContentMock() http.Handler

	// MethodsSplitter - разделяет поток обработки в зависимости от метода
	MethodsSplitter(handlers map[string]http.Handler) http.Handler
	// PermissionTDHandler - обработчик  прав с рендерингом шаблонной страницы
	PermissionJSONHandler(next http.Handler, code string, lv int16) http.Handler
	// PermissionJSONHandler - обработчик  прав с JSON ответом
	PermissionTDHandler(next http.Handler, code string, lv int16) http.Handler
	// PublicCacheMiddleware - оборачивает ответ заголовками публичного кеша
	PublicCacheMiddleware(next http.Handler) http.Handler

	// TemplateData - создает и наполняет основные данные для шаблона
	TemplateData(ctx context.Context, name link.PageTemplateName) webServerDTO.TemplateData

	WriteJSON(ctx context.Context, w http.ResponseWriter, statusCode int, data interface{})
	WriteNoContent(w http.ResponseWriter)
	WritePlain(ctx context.Context, w http.ResponseWriter, statusCode int, text string)
	WriteTemplate(ctx context.Context, w http.ResponseWriter, statusCode int, data webServerDTO.TemplateData)

	// AddCategory - добавляет раздел в меню
	AddCategory(ctx context.Context, category webServerDTO.MenuCategoryInternal) error
	// FilterMainMenu - производит фильтрацию элементов главного меню
	FilterMainMenu(ctx context.Context, state webServerDTO.State) []webServerDTO.MenuItem
	// FilterFullMenu - производит фильтрацию элементов полного меню
	FilterFullMenu(ctx context.Context, state webServerDTO.State) []webServerDTO.MenuCategory

	// RegisterModule - регистрирует веб модуль
	RegisterModule(ctx context.Context, module webServerDTO.Module)
	// HasModule - проверяет наличие веб модуля
	HasModule(ctx context.Context, name string) bool
	// ActiveModules - информация об активных модулях
	ActiveModules(ctx context.Context) []webServerDTO.ModuleInfo

	// GeneratePagination - генерирует страницы для пагинатора
	GeneratePagination(currentPage int64, pageCount int64, linkGenerator func(pageNumber int64) string) []webServerDTO.Page
}
