package ctxench

type CtxKey struct {
	Some string
}

var (
	debugModeKey     = &CtxKey{"debugModeKey"}
	requestIDKey     = &CtxKey{"requestIDKey"}
	connectionIDKey  = &CtxKey{"connectionIDKey"}
	serverIDKey      = &CtxKey{"serverIDKey"}
	originPathKey    = &CtxKey{"originPathKey"}
	developerModeKey = &CtxKey{"developerModeKey"}
)
