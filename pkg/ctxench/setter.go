package ctxench

import (
	"context"
	"crypto/md5"
	"fmt"
	"time"
)

func randomStringID() string {
	// TODO: не годится для нагрузок
	return fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String())))
}

func WithRequestID(ctx context.Context) context.Context {
	return context.WithValue(ctx, requestIDKey, randomStringID())
}

func WithConnectionID(ctx context.Context) context.Context {
	return context.WithValue(ctx, connectionIDKey, randomStringID())
}

func WithServerID(ctx context.Context) context.Context {
	return context.WithValue(ctx, serverIDKey, randomStringID())
}

func WithDebugMode(ctx context.Context, debug bool) context.Context {
	return context.WithValue(ctx, debugModeKey, debug)
}

func WithOriginPath(ctx context.Context, path string) context.Context {
	return context.WithValue(ctx, originPathKey, path)
}

func WithDeveloperMode(ctx context.Context) context.Context {
	return context.WithValue(ctx, developerModeKey, true)
}
