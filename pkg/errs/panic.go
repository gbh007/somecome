package errs

import (
	"errors"
	"fmt"
)

var PanicFoundErr = errors.New("found panic")

func FromPanic(p interface{}) error {
	if p == nil {
		return nil
	}

	return AddTrace(fmt.Errorf("%w: %v", PanicFoundErr, p), 0)
}
