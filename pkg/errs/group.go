package errs

import "errors"

// Group - категория/группа ошибок
type Group interface {
	// Группа это тоже ошибка, поэтому должна реализовать интерфейс ошибки
	error
	// Name - название группы
	Name() string
}

// group - реализация интерфейса Group
type group struct {
	// Встраивание ошибки
	error
	// Название группы
	name string
}

// Name - возвращает название группы
func (g group) Name() string { return g.name }

// NewGroup - создает новую группу
func NewGroup(name string) Group {
	return &group{
		error: errors.New(name),
		name:  name,
	}
}
