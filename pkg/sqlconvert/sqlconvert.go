package sqlconvert

import (
	"database/sql"
	"time"
)

func FromNullTime(src sql.NullTime) *time.Time {
	if !src.Valid {
		return nil
	}

	return &src.Time
}

func ToNullTime(src *time.Time) sql.NullTime {
	if src == nil {
		return sql.NullTime{}
	}

	return sql.NullTime{
		Time:  *src,
		Valid: true,
	}
}

func FromNullInt64(src sql.NullInt64) *int64 {
	if !src.Valid {
		return nil
	}

	return &src.Int64
}

func ToNullInt64(src *int64) sql.NullInt64 {
	if src == nil {
		return sql.NullInt64{}
	}

	return sql.NullInt64{
		Int64: *src,
		Valid: true,
	}
}

func FromNullFloat64(src sql.NullFloat64) *float64 {
	if !src.Valid {
		return nil
	}

	return &src.Float64
}

func ToNullFloat64(src *float64) sql.NullFloat64 {
	if src == nil {
		return sql.NullFloat64{}
	}

	return sql.NullFloat64{
		Float64: *src,
		Valid:   true,
	}
}

func ToNullString(src *string) sql.NullString {
	if src == nil {
		return sql.NullString{}
	}

	return sql.NullString{
		String: *src,
		Valid:  true,
	}
}

func FromNullString(src sql.NullString) *string {
	if !src.Valid {
		return nil
	}

	return &src.String
}

func ToNullStringE(src string) sql.NullString {
	if src == "" {
		return sql.NullString{}
	}

	return sql.NullString{
		String: src,
		Valid:  true,
	}
}
