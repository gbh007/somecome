-- Схема с данными коментариев на контент
CREATE SCHEMA comment;

-- Сообщения
CREATE TABLE comment.messages(
    id              SERIAL8     PRIMARY KEY,
    content_key     TEXT        NOT NULL,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    raw_text        TEXT        NOT NULL,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Вложения в сообщение
CREATE TABLE comment.attachments(
    message_id      INT8        NOT NULL REFERENCES comment.messages (id),
    file_token      TEXT        NOT NULL REFERENCES file.files (token),
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);