-- Схема с правами и авторизацией
CREATE SCHEMA auth;

-- Данные пользователя
CREATE TABLE auth.users(
    id          SERIAL8     PRIMARY KEY,
    login       TEXT        NOT NULL UNIQUE,
    password    TEXT        NOT NULL,
    salt        TEXT        NOT NULL,
    created     TIMESTAMPTZ NOT NULL,
    updated     TIMESTAMPTZ
);

-- Сессии пользователей
CREATE TABLE auth.sessions(
    token       TEXT                    PRIMARY KEY,
    user_id     INT8        NOT NULL    REFERENCES auth.users (id),
    is_closed   BOOLEAN     NOT NULL    DEFAULT FALSE,
    created     TIMESTAMPTZ NOT NULL,
    used        TIMESTAMPTZ,
    updated     TIMESTAMPTZ
);
