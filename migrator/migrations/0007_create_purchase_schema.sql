-- Схема с данными покупок
CREATE SCHEMA purchase;

-- Категории товара
CREATE TABLE purchase.categories(
    id              SERIAL8     PRIMARY KEY,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    name            TEXT        NOT NULL,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Карты (способы оплаты)
CREATE TABLE purchase.cards(
    id              SERIAL8     PRIMARY KEY,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    name            TEXT        NOT NULL,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Организации (магазин)
CREATE TABLE purchase.organizations(
    id              SERIAL8     PRIMARY KEY,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    name            TEXT        NOT NULL,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Покупки
CREATE TABLE purchase.purchases(
    id                  SERIAL8             PRIMARY KEY,
    creator_id          INT8                NOT NULL REFERENCES auth.users (id),
    organization_id     INT8                REFERENCES purchase.organizations (id),
    payment_at          TIMESTAMPTZ,
    total               NUMERIC(12,2),
    created             TIMESTAMPTZ         NOT NULL,
    updated             TIMESTAMPTZ
);

-- Купленые товары
CREATE TABLE purchase.purchase_items(
    id              SERIAL8             PRIMARY KEY,
    creator_id      INT8                NOT NULL REFERENCES auth.users (id),
    purchase_id     INT8                REFERENCES purchase.purchases (id) NOT NULL,
    category_id     INT8                REFERENCES purchase.categories (id),
    name            TEXT                NOT NULL,
    price           NUMERIC(12,2)       NOT NULL,
    amount          NUMERIC(12,2)       NOT NULL,
    sale            NUMERIC(12,2)       NOT NULL,
    total           NUMERIC(12,2)       NOT NULL,
    created         TIMESTAMPTZ         NOT NULL,
    updated         TIMESTAMPTZ
);

-- Оплата покупки
CREATE TABLE purchase.purchase_payments(
    id              SERIAL8             PRIMARY KEY,
    creator_id      INT8                NOT NULL REFERENCES auth.users (id),
    purchase_id     INT8                REFERENCES purchase.purchases (id),
    card_id         INT8                REFERENCES purchase.cards (id),
    total           NUMERIC(12,2)       NOT NULL,
    created         TIMESTAMPTZ         NOT NULL,
    updated         TIMESTAMPTZ
);