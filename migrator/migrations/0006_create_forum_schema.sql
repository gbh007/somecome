-- Схема с данными формуа
CREATE SCHEMA forum;

-- Основные данные форума
CREATE TABLE forum.forums(
    id              SERIAL8     PRIMARY KEY,
    creator_id      INT8        NOT NULL REFERENCES auth.users (id),
    name            TEXT        NOT NULL,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);