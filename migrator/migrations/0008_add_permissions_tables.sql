-- Домены для прав - служат для разграничиения большого количества привелегий
CREATE TABLE auth.domains(
    code            TEXT        PRIMARY KEY,
    name            TEXT        NOT NULL,
    description     TEXT,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Добавление доменов
INSERT INTO auth.domains(code, "name", "description", "created") VALUES ('auth', 'Контроль доступа', 'Домен прав для конфигурации авторизации', NOW());
INSERT INTO auth.domains(code, "name", "description", "created") VALUES ('forum', 'Форум', 'Домен прав для форума', NOW());
INSERT INTO auth.domains(code, "name", "description", "created") VALUES ('purchase', 'Покупки', 'Домен прав для покупок', NOW());

-- Уровни права
CREATE TABLE auth.permission_levels(
    level           INT2        PRIMARY KEY,
    name            TEXT        NOT NULL
);

-- добавление уровней прав
INSERT INTO auth.permission_levels(level, "name") VALUES (0, 'Запрет');
INSERT INTO auth.permission_levels(level, "name") VALUES (1, 'Только чтение');
INSERT INTO auth.permission_levels(level, "name") VALUES (2, 'Чтение и запись');
INSERT INTO auth.permission_levels(level, "name") VALUES (3, 'Чтение, запись, удаление');

-- Права
CREATE TABLE auth.permissions(
    id              SERIAL8     PRIMARY KEY,
    domain_code     TEXT        NOT NULL REFERENCES auth.domains (code),
    code            TEXT        NOT NULL UNIQUE,
    name            TEXT        NOT NULL,
    description     TEXT,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Добавление прав
INSERT INTO auth.permissions(domain_code, code, "name", "description", "created") VALUES ('auth', 'auth-permission-control', 'Управление правами пользователей', 'Выдача прав пользователю, управление группами прав', NOW());
INSERT INTO auth.permissions(domain_code, code, "name", "description", "created") VALUES ('auth', 'auth-registration', 'Регистрация нового пользователя', 'Управление регистрацией нового пользователя', NOW());
INSERT INTO auth.permissions(domain_code, code, "name", "description", "created") VALUES ('forum', 'forum-access', 'Доступ до форума', 'Базовый доступ пользователя до форума', NOW());
INSERT INTO auth.permissions(domain_code, code, "name", "description", "created") VALUES ('purchase', 'purchase-access', 'Доступ до покупок', 'Базовый доступ пользователя до покупок', NOW());

-- Группы пользователей
CREATE TABLE auth.groups(
    id              SERIAL8     PRIMARY KEY,
    name            TEXT        NOT NULL,
    description     TEXT,
    auto_append     BOOLEAN     NOT NULL DEFAULT FALSE,
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

-- Права выданные в группе
CREATE TABLE auth.group_permissions(
    group_id        INT8        NOT NULL    REFERENCES auth.groups (id),
    permission_id   INT8        NOT NULL    REFERENCES auth.permissions (id),
    level           INT2        NOT NULL    REFERENCES auth.permission_levels (level),
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ,
    UNIQUE(group_id, permission_id)
);


-- Привязка пользователей к группе
CREATE TABLE auth.group_users(
    group_id    INT8        NOT NULL    REFERENCES auth.groups (id),
    user_id     INT8        NOT NULL    REFERENCES auth.users (id),
    created     TIMESTAMPTZ NOT NULL,
    UNIQUE(user_id, group_id)
);

-- Права для неавторизованных пользователей
CREATE TABLE auth.permissions_of_unauthorized(
    permission_id   INT8        NOT NULL    UNIQUE REFERENCES auth.permissions (id),
    level           INT2        NOT NULL    REFERENCES auth.permission_levels (level),
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);