-- Схема с файлами
CREATE SCHEMA file;

-- Файлы
CREATE TABLE file.files(
    token       TEXT                    PRIMARY KEY,
    loaded      BOOLEAN     NOT NULL    DEFAULT FALSE,
    user_id     INT8        NOT NULL    REFERENCES auth.users (id),
    name        TEXT,
    mime        TEXT,
    size        INT8,
    created     TIMESTAMPTZ NOT NULL,
    used        TIMESTAMPTZ,
    updated     TIMESTAMPTZ
);