-- Схема с данными пользователей
CREATE SCHEMA user_info;

-- Основные данные пользователя
CREATE TABLE user_info.main_info(
    user_id         INT8        NOT NULL PRIMARY KEY    REFERENCES auth.users (id),
    origin_login    TEXT,
    firstname       TEXT,
    secondname      TEXT,
    patronymic      TEXT,
    avatar          TEXT        REFERENCES file.files (token),
    created         TIMESTAMPTZ NOT NULL,
    updated         TIMESTAMPTZ
);

