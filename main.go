package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"somecome/config"
	"somecome/domain/auth"
	"somecome/domain/comment"
	"somecome/domain/content"
	"somecome/domain/file"
	"somecome/domain/forum"
	"somecome/domain/purchase"
	"somecome/domain/reaction"
	"somecome/domain/txtp"
	"somecome/domain/user"
	"somecome/domain/webserver"
	"somecome/inter"
	"somecome/migrator"
	"somecome/pkg/ctxench"
	"somecome/pkg/logger"
	"somecome/superobject/application"
	"syscall"
	"time"
)

func main() {
	developerMode := flag.Bool("developer", false, "активировать режим разработчика")
	configPath := flag.String("c", "config.json", "путь файла конфигурации")
	flag.Parse()

	ctx, cancelNotify := signal.NotifyContext(
		context.Background(),
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)
	defer cancelNotify()

	// Переходим в режим разработчика
	if *developerMode {
		ctx = ctxench.WithDeveloperMode(ctx)
		logger.Warning(ctx, "Система переведена в режим разработчика")
	}

	cfg, err := config.Load(ctx, *configPath)
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(10)
	}

	if cfg.System == nil {
		logger.ErrorText(ctx, "system config not exists")
		os.Exit(11)
	}

	if cfg.System.DebugMode {
		ctx = ctxench.WithDebugMode(ctx, true)
	}

	rawDB, err := inter.NewStorage(ctx, inter.Config{
		Host:     cfg.DB.Host,
		Port:     cfg.DB.Port,
		User:     cfg.DB.User,
		Password: cfg.DB.Password,
		DBName:   cfg.DB.DBName,
	})
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(12)
	}

	// Накатываем миграции
	err = migrator.MigrateAll(ctx, rawDB, true)
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(13)
	}

	app := new(application.Application)

	app.AddDomains(
		new(auth.Domain),
		new(file.Domain),
		new(user.Domain),
		new(comment.Domain),
		new(reaction.Domain),
		new(forum.Domain),
		new(purchase.Domain),
		new(webserver.Domain),
		new(txtp.Domain),
		new(content.Domain),
	)

	err = app.Configure(ctx, rawDB, cfg)
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(24)
	}

	err = app.BaseInit(ctx)
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(25)
	}

	err = app.ExtendInit(ctx)
	if err != nil {
		logger.Error(ctx, err)
		os.Exit(26)
	}

	exit, err := app.Start(ctx)
	if err != nil {
		logger.Error(ctx, err)

		cancelNotify()

		select {
		case <-exit:
			os.Exit(27)
		case <-time.After(time.Minute):
			os.Exit(28)
		case <-time.Tick(time.Second * 5):
			logger.Warning(ctx, "Остановка приложения")
		}

	}

	<-exit

}
