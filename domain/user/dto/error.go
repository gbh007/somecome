package userDTO

import "errors"

var (
	// Пользователя не существует (не найден)
	UserNotFoundErr = errors.New("user not found")
	// Смена логина невозможна
	LoginChangeIsNotPossibleErr = errors.New("login change is not possible")
)
