package user

import (
	"context"
	"somecome/config"
	"somecome/domain/user/database"
	"somecome/domain/user/userWebModule"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с данными пользователей
type Domain struct {
	// База данных
	storage *database.Database
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *userWebModule.Module
}

// Name - название домена
func (_ *Domain) Name() string { return "user" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = userWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.storage = database.New(db)

	return nil
}
