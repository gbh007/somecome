package pageHandlers

import (
	"errors"
	"net/http"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

// UserEditTD - данные пользователя для рендеринга
type UserEditTD struct {
	// ИД пользователя
	ID int64
	// Логин пользователя
	Login string

	// Имя пользователя
	Firstname string
	// Фамилия пользователя
	Secondname string
	// Отчество пользователя
	Patronymic string

	// Ссылка для сохранения
	SaveLink string
}

func (o *Object) userEditPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, UserEditPageTemplateName)

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := UserEditTD{
			ID:       userData.ID,
			Login:    userData.Login,
			SaveLink: PathToUserEdit,
		}

		userInfo, err := o.superObject.UserDomain().GetMainInfo(ctx, userData.ID)
		if err != nil && !errors.Is(err, userDTO.UserNotFoundErr) {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		if userInfo != nil && userInfo.OriginLogin != nil {
			data.Login = *userInfo.OriginLogin
		}

		if userInfo != nil && userInfo.Firstname != nil {
			data.Firstname = *userInfo.Firstname
		}

		if userInfo != nil && userInfo.Secondname != nil {
			data.Secondname = *userInfo.Secondname
		}

		if userInfo != nil && userInfo.Patronymic != nil {
			data.Patronymic = *userInfo.Patronymic
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) userEditPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		login := r.FormValue("login")
		secondname := r.FormValue("secondname")
		firstname := r.FormValue("firstname")
		patronymic := r.FormValue("patronymic")

		err = o.superObject.UserDomain().UpdateMainInfo(ctx, userData.ID, userDTO.UpdateMainInfoInput{
			OriginLogin: login,
			Firstname:   firstname,
			Secondname:  secondname,
			Patronymic:  patronymic,
		})
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		http.Redirect(w, r, PathToUserInfo, http.StatusSeeOther)
	})
}
