package user

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена с пользовательскими данными
	DomainError = errs.NewGroup("user domain")
)
