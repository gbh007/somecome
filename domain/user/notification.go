package user

import (
	"context"
	"somecome/domain/user/database"
	userDTO "somecome/domain/user/dto"
	"somecome/pkg/errs"
	"somecome/pkg/paginator"
	"somecome/pkg/sqlconvert"
)

// GetNotifications - возвращает список уведомлений пользователя
func (domain *Domain) GetNotifications(ctx context.Context, userID int64, page *int64) (*userDTO.Notifications, error) {
	notifications := new(userDTO.Notifications)

	var (
		rawNotifications []*database.Notification
		err              error
	)

	notifications.Count, err = domain.storage.SelectNotificationCount(ctx, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	notifications.PageCount = paginator.TotalToPages(notifications.Count, paginator.DefaultOnPageCount)

	notifications.UnreadCount, err = domain.storage.SelectUnreadNotificationCount(ctx, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	if page != nil {
		limit, offset := paginator.PageToLimit(*page, paginator.DefaultOnPageCount)
		rawNotifications, err = domain.storage.SelectNotificationsWithLimit(ctx, userID, limit, offset)
	} else {
		rawNotifications, err = domain.storage.SelectNotifications(ctx, userID)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	for _, notification := range rawNotifications {
		notifications.Items = append(notifications.Items, fromModel(notification))
	}

	return notifications, nil
}

// GetUnreadNotificationCount - возвращает количество непрочитанных уведомлений пользователя
func (domain *Domain) GetUnreadNotificationCount(ctx context.Context, userID int64) (int64, error) {
	unreadCount, err := domain.storage.SelectUnreadNotificationCount(ctx, userID)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return unreadCount, nil
}

// MarkNotificationAsRead - отмечает уведомление прочитанным
func (domain *Domain) MarkNotificationAsRead(ctx context.Context, notificationID, userID int64) error {
	err := domain.storage.UpdateNotificationReadByID(ctx, notificationID, userID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// MarkNotificationAsReadByUser - отмечает уведомления пользователя прочитанными
func (domain *Domain) MarkNotificationAsReadByUser(ctx context.Context, userID int64) error {
	err := domain.storage.UpdateNotificationReadByUser(ctx, userID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// CreateNotification - создает новое уведомление
func (domain *Domain) CreateNotification(ctx context.Context, notification userDTO.NotificationInput) (int64, error) {
	id, err := domain.storage.InsertNotification(ctx, &database.Notification{
		UserID:     notification.UserID,
		Title:      notification.Title,
		Message:    notification.Message,
		ContentKey: sqlconvert.ToNullStringE(notification.ContentKey),
		Link:       sqlconvert.ToNullStringE(notification.Link),
		LinkText:   sqlconvert.ToNullStringE(notification.LinkText),
	})
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}

func fromModel(raw *database.Notification) *userDTO.Notification {
	return &userDTO.Notification{
		ID:         raw.ID,
		UserID:     raw.UserID,
		Title:      raw.Title,
		Message:    raw.Message,
		ContentKey: sqlconvert.FromNullString(raw.ContentKey),
		Link:       sqlconvert.FromNullString(raw.Link),
		LinkText:   sqlconvert.FromNullString(raw.LinkText),
		IsRead:     raw.IsRead,
		Created:    raw.Created,
	}
}
