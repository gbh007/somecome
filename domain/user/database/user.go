package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
	"time"
)

// UserInfo - данные пользователя
type UserInfo struct {
	// ИД пользователя
	UserID int64 `db:"user_id"`
	// Оригинальный логин пользователя
	OriginLogin sql.NullString `db:"origin_login"`
	// Имя пользователя
	Firstname sql.NullString `db:"firstname"`
	// Фамилия пользователя
	Secondname sql.NullString `db:"secondname"`
	// Отчество пользователя
	Patronymic sql.NullString `db:"patronymic"`
	// Токен аватара пользователя
	Avatar sql.NullString `db:"avatar"`
	// Время создания записи
	Created time.Time `db:"created"`
	// Время последнего обновления записи
	Updated sql.NullTime `db:"updated"`
}

// CreateUserInfo - добавляет информацию о пользователе в базу.
// Поле Updated игнорируется, поле Created заменяется
func (d *Database) CreateUserInfo(ctx context.Context, user *UserInfo) error {
	user.Created = time.Now()

	_, err := d.db.NamedExecContext(ctx, `INSERT INTO "user".main_info 
			(user_id, origin_login, firstname, secondname, patronymic, avatar, created)
	VALUES
			(:user_id, :origin_login, :firstname, :secondname, :patronymic, :avatar, :created);`, user)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// GetUserInfoByID - возвращает данные пользователя по ИД
func (d *Database) GetUserInfoByID(ctx context.Context, id int64) (*UserInfo, error) {
	user := new(UserInfo)

	err := d.db.GetContext(ctx, user, `SELECT * FROM "user".main_info WHERE user_id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return user, nil
}

// UpdateUserInfo - обновляет информацию о пользователе в базе.
// Поле Created игнорируется, поле Updated заменяется
func (d *Database) UpdateUserInfo(ctx context.Context, user *UserInfo) error {
	tNow := time.Now()
	user.Updated = sqlconvert.ToNullTime(&tNow)

	_, err := d.db.NamedExecContext(ctx, `UPDATE "user".main_info SET
			origin_login = :origin_login,
			firstname = :firstname,
			secondname = :secondname,
			patronymic = :patronymic,
			avatar = :avatar,
			updated = :updated
	WHERE user_id = :user_id;`, user)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// GetAllUserInfo - возвращает данные всех пользователей
func (d *Database) GetAllUserInfo(ctx context.Context) ([]*UserInfo, error) {
	users := make([]*UserInfo, 0)

	err := d.db.SelectContext(ctx, &users, `SELECT * FROM "user".main_info;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return users, nil
}
