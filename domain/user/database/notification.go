package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Notification - данные для уведомления
type Notification struct {
	// ИД уведомления
	ID int64 `db:"id"`
	// ИД пользователя получившего уведомление
	UserID int64 `db:"user_id"`
	// Заголовок уведомления
	Title string `db:"title"`
	// Текст уведомления
	Message string `db:"message"`
	// Ключ контента
	ContentKey sql.NullString `db:"content_key"`
	// Ссылка в уведомлении
	Link sql.NullString `db:"link"`
	// Текст ссылки
	LinkText sql.NullString `db:"link_text"`
	// Уведомление прочитано
	IsRead bool `db:"is_read"`
	// Время создания уведомления
	Created time.Time `db:"created"`
}

// InsertNotification - добавляет новое уведомление
func (d *Database) InsertNotification(ctx context.Context, notification *Notification) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO "user".notification
			(user_id, title, message, content_key, link, link_text, created)
	VALUES
			($1, $2, $3, $4, $5, $6, $7);`,
		notification.UserID, notification.Title, notification.Message,
		notification.ContentKey, notification.Link, notification.LinkText,
		time.Now(),
	)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdateNotificationReadByID - обновляет статус прочтения уведомления по его ID
func (d *Database) UpdateNotificationReadByID(ctx context.Context, notificationID, userID int64) error {
	_, err := d.db.ExecContext(ctx, `UPDATE "user".notification SET is_read = TRUE WHERE id = $1 AND user_id = $2;`, notificationID, userID)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdateNotificationReadByUser - обновляет статус прочтения уведомления по пользователю и времени
func (d *Database) UpdateNotificationReadByUser(ctx context.Context, userID int64) error {
	_, err := d.db.ExecContext(ctx, `UPDATE "user".notification SET is_read = TRUE WHERE user_id = $1;`, userID)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectNotificationsWithLimit - получает список уведомлений для пользователя, с ограничениями
func (d *Database) SelectNotificationsWithLimit(ctx context.Context, userID int64, limit, offset int64) ([]*Notification, error) {
	list := make([]*Notification, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM "user".notification WHERE user_id = $1 ORDER BY created DESC LIMIT $2 OFFSET $3;`, userID, limit, offset)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectNotifications - получает список уведомлений для пользователя
func (d *Database) SelectNotifications(ctx context.Context, userID int64) ([]*Notification, error) {
	list := make([]*Notification, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM "user".notification WHERE user_id = $1 ORDER BY created DESC;`, userID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectNotificationCount - получает количество уведомлений для пользователя
func (d *Database) SelectNotificationCount(ctx context.Context, userID int64) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT count(*) FROM "user".notification WHERE user_id = $1;`, userID)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}

// SelectUnreadNotificationCount - получает количество непрочитанных уведомлений для пользователя
func (d *Database) SelectUnreadNotificationCount(ctx context.Context, userID int64) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT count(*) FROM "user".notification WHERE user_id = $1 AND is_read = FALSE;`, userID)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}
