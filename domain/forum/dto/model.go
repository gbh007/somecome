package forumDTO

import (
	"time"
)

// Forum - данные о форуме
type Forum struct {
	// ИД форума
	ID int64
	// Ид пользователя создавшего форум
	CreatorID int64
	// Название форума
	Name string
	// Время создания форума
	Created time.Time
	// Время последнего обновления форума
	Updated *time.Time

	// Ключ контента
	ContentKey string

	// Количество сообщений
	MessageCount int64
	// Количество страниц
	PageCount int64
}
