package pageHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/forum/forumShared"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToCreateForum, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.forumCreatePageGet(wsd), forumShared.ForumAccessPC, authDTO.LevelReadOnly),
			http.MethodPost: wsd.PermissionTDHandler(o.forumCreatePagePost(wsd), forumShared.ForumAccessPC, authDTO.LevelReadWrite),
		},
	))
	mux.Handle(PathToForumList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.forumListPageGet(wsd), forumShared.ForumAccessPC, authDTO.LevelReadOnly),
		},
	))
	mux.Handle(PathToForumPage, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.forumPageGet(wsd), forumShared.ForumAccessPC, authDTO.LevelReadOnly),
		},
	))
}
