package pageHandlers

import (
	"context"
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:forum:page"

// Коды страниц для рендеринга шаблонов
var (
	ForumCreatePageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:forum:page:create-forum"}
	ForumListPageName   = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:forum:page:forums"}
	ForumPageName       = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:forum:page:forum"}
)

// Ендпоинты веб сервера
const (
	PathToCreateForum = "/pages/forum/create"
	PathToForumPage   = "/pages/forum"
	PathToForumList   = "/pages/forum/list"
)

// GetForumPageLink - генерирует ссылку на страницу форума
func GetForumPageLink(forumID int64, pageNumber *int64, toLast bool) string {
	link := url.URL{
		Path: PathToForumPage,
	}

	args := url.Values{
		"id": []string{strconv.FormatInt(forumID, 10)},
	}

	switch {
	case pageNumber != nil:
		args.Set("page", strconv.FormatInt(*pageNumber, 10))
	case toLast:
		args.Set("page", "last")
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetForumPageLinkWrapper - враппер для генерации ссылок на страницы форума
func GetForumPageLinkWrapper(forumID int64, pageNumber *int64, toLast bool) link.PageLinkGetter {
	return func(pageNumber int64) string {
		return GetForumPageLink(forumID, &pageNumber, false)
	}
}

// GetForumLink - генерирует ссылку на форума
func GetForumLink(_ context.Context, forumID string) string {
	link := url.URL{
		Path: PathToForumPage,
	}

	args := url.Values{
		"id":   []string{forumID},
		"page": []string{"last"},
	}

	link.RawQuery = args.Encode()

	return link.String()
}
