package pageHandlers

import (
	forumDTO "somecome/domain/forum/dto"
	"time"
)

// ForumListTD - данные для рендеринга списка форумов
type ForumListTD struct {
	// Список форумов
	Forums []ForumTD
	// Ссылка на создание нового форума
	NewLink string
}

// ForumTD - данные о форуме
type ForumTD struct {
	// ИД форума
	ID int64
	// Адрес форума
	URL string
	// Название форума
	Name string
	// Время создания форума
	Created time.Time

	// Количество сообщений
	MessageCount int64
	// Количество страниц
	PageCount int64

	// Ключ контента
	ContentKey string
}

func forumTDFromModel(raw *forumDTO.Forum) ForumTD {
	return ForumTD{
		ID:           raw.ID,
		URL:          GetForumPageLink(raw.ID, nil, false),
		Name:         raw.Name,
		Created:      raw.Created,
		MessageCount: raw.MessageCount,
		PageCount:    raw.PageCount,
		ContentKey:   raw.ContentKey,
	}
}

// ForumPageTD - данные для страницы с форумом
type ForumPageTD struct {
	// Данные о форуме
	Info ForumTD
	// Текущая страница
	CurrentPage int64
	// Ключ контента
	ContentKey string
}
