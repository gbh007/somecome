package forum

import (
	"context"
	"somecome/config"
	authDTO "somecome/domain/auth/dto"
	contentDTO "somecome/domain/content/dto"
	"somecome/domain/forum/database"
	"somecome/domain/forum/forumShared"
	"somecome/domain/forum/forumWebModule"
	"somecome/domain/forum/forumWebModule/pageHandlers"
	"somecome/pkg/errs"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

const ckPrefix = "forum-"

// Domain - домен для работы с форумом
type Domain struct {
	// БД
	storage *database.Database
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *forumWebModule.Module
}

// Name - название домена
func (_ *Domain) Name() string { return "forum" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = forumWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	err := domain.superObject.ContentDomain().RegisterKey(ctx, contentDTO.ContentKey{
		Prefix:         ckPrefix,
		GetLink:        pageHandlers.GetForumLink,
		GetName:        domain.forumNameToContent,
		GetDescription: domain.forumDescriptionToContent,
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	err = domain.superObject.AuthDomain().RegisterPermissions(ctx, authDTO.RegisterDomainInput{
		Code:        domain.Name(),
		Name:        "Форум",
		Description: "Домен прав для форума",
		Permissions: []authDTO.DomainPermission{
			{
				Code:        forumShared.ForumAccessPC,
				Name:        "Доступ до форума",
				Description: "Базовый доступ пользователя до форума",
			},
		},
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.storage = database.New(db)

	return nil
}
