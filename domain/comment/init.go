package comment

import (
	"context"
	"somecome/config"
	"somecome/domain/comment/commentWebModule"
	"somecome/domain/comment/database"
	contentDTO "somecome/domain/content/dto"
	"somecome/pkg/errs"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

const ckPrefix = "comment-"

// Domain - домен для работы с комментариями
type Domain struct {
	// БД
	storage *database.Database
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	webModule *commentWebModule.Module
}

// Name - название домена
func (_ *Domain) Name() string { return "comment" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.webModule = commentWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.webModule)

	// TODO: на данный момент нельзя просмотреть комментарий отдельно, необходимо реализовать
	err := domain.superObject.ContentDomain().RegisterKey(ctx, contentDTO.ContentKey{Prefix: ckPrefix})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.storage = database.New(db)

	return nil
}
