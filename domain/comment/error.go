package comment

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена комментариев
	DomainError = errs.NewGroup("comment domain")
)
