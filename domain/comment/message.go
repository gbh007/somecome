package comment

import (
	"context"
	"fmt"
	"somecome/domain/comment/database"
	commentDTO "somecome/domain/comment/dto"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/errs"
	"somecome/pkg/paginator"
	"somecome/pkg/sqlconvert"
)

// NewMessage - создает новое сообщение на контент
func (domain *Domain) NewMessage(ctx context.Context, message *commentDTO.MessageInput) (int64, error) {
	messageID, err := domain.storage.CreateMessage(
		ctx,
		message.ContentKey,
		message.RawText,
		sqlconvert.ToNullString(message.Dialect),
		message.CreatorID,
	)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	for _, attachment := range message.Attachments {
		fileToken, err := domain.superObject.FileDomain().UploadFileFull(ctx, message.CreatorID, fileDTO.FileInput{
			Name: attachment.Name,
			Mime: attachment.Mime,
			Body: attachment.Body,
		})
		if err != nil {
			return 0, errs.WrapError(ctx, DomainError, err)
		}

		err = domain.storage.CreateAttachment(ctx, messageID, fileToken, message.CreatorID)
		if err != nil {
			return 0, errs.WrapError(ctx, DomainError, err)
		}
	}

	return messageID, nil
}

// Messages - возвращает список сообщений на контент
func (domain *Domain) Messages(ctx context.Context, contentKey string, page *int64) ([]*commentDTO.Message, error) {
	var (
		rawMessages []*database.Message
		err         error
	)

	if page != nil {
		limit, offset := paginator.PageToLimit(*page, paginator.DefaultOnPageCount)
		rawMessages, err = domain.storage.SelectMessagesWithLimit(ctx, contentKey, limit, offset)
	} else {
		rawMessages, err = domain.storage.SelectMessages(ctx, contentKey)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	list := make([]*commentDTO.Message, 0)

	for _, rawInfo := range rawMessages {
		msg, err := domain.messageFromRaw(ctx, rawInfo, true)
		if err != nil {
			return nil, errs.WrapError(ctx, DomainError, err)
		}

		list = append(list, msg)
	}

	return list, nil
}

// messageFromRaw - конвертирует сообщение из формата БД, с добавлением всего необходимого
func (domain *Domain) messageFromRaw(ctx context.Context, raw *database.Message, includeAttachment bool) (*commentDTO.Message, error) {
	msg := &commentDTO.Message{
		ID:                 raw.ID,
		LinkedToContentKey: raw.ContentKey,
		CreatorID:          raw.CreatorID,
		RawText:            raw.RawText,
		Dialect:            sqlconvert.FromNullString(raw.Dialect),
		Created:            raw.Created,
		Updated:            sqlconvert.FromNullTime(raw.Updated),
		InnerContentKey:    fmt.Sprintf("%s%d", ckPrefix, raw.ID),
	}

	if !includeAttachment {
		return msg, nil
	}

	rawAttachments, err := domain.storage.SelectAttachments(ctx, msg.ID)
	if err != nil {
		return nil, err
	}

	for _, rawInfo := range rawAttachments {
		msg.Attachments = append(msg.Attachments, commentDTO.Attachment{
			MessageID: rawInfo.MessageID,
			FileToken: rawInfo.FileToken,
			CreatorID: rawInfo.CreatorID,
			Created:   raw.Created,
			Updated:   sqlconvert.FromNullTime(raw.Updated),
		})
	}

	return msg, nil
}

// MessageCount - возвращает количество сообщений на контент и количество страниц
func (domain *Domain) MessageCount(ctx context.Context, contentKey string) (int64, int64, error) {
	count, err := domain.storage.SelectMessageCount(ctx, contentKey)
	if err != nil {
		return 0, 0, errs.WrapError(ctx, DomainError, err)
	}

	return count, paginator.TotalToPages(count, paginator.DefaultOnPageCount), nil
}
