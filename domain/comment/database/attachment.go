package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// CreateAttachment - создает запись о вложении в сообщение
func (d *Database) CreateAttachment(ctx context.Context, messageID int64, fileToken string, userID int64) error {
	_, err := d.db.ExecContext(ctx, `INSERT INTO comment.attachments(message_id, file_token, creator_id, created)
	 VALUES ($1, $2, $3, $4);`, messageID, fileToken, userID, time.Now())
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectAttachments - получает список вложений для сообщения
func (d *Database) SelectAttachments(ctx context.Context, messageID int64) ([]*Attachment, error) {
	list := make([]*Attachment, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM comment.attachments WHERE message_id = $1;`, messageID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}
