package comment

import (
	"context"
	commentDTO "somecome/domain/comment/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

// Message - возвращает сообщение на контент, по его ID
func (domain *Domain) Message(ctx context.Context, id int64, includeAttachments bool) (*commentDTO.Message, error) {
	rawMessage, err := domain.storage.SelectMessage(ctx, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	message, err := domain.messageFromRaw(ctx, rawMessage, includeAttachments)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return message, nil
}

// UpdateMessageText - обновляет текст сообщения, по его ID
func (domain *Domain) UpdateMessageText(ctx context.Context, userID, messageID int64, text string, dialect *string) error {
	rawMessage, err := domain.storage.SelectMessage(ctx, messageID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Только автор может изменить сообщение
	if rawMessage.CreatorID != userID {
		return errs.WrapError(ctx, DomainError, commentDTO.NotAuthorErr)
	}

	rawMessage.RawText = text
	rawMessage.Dialect = sqlconvert.ToNullString(dialect)

	err = domain.storage.UpdateMessage(ctx, rawMessage)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}
