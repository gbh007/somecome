package pageHandlers

import (
	"net/http"
	txtpDTO "somecome/domain/txtp/dto"
	"somecome/domain/webserver/core"
	"somecome/superobject"
	"strconv"
)

// MessageEditPageTD - данные для страницы изменения комментария
type MessageEditPageTD struct {
	// ИД сообщения
	MessageID int64
	// Текст сообщения
	Text string
	// Диалект
	Dialect string
	// Диалекты сообщения
	Dialects []string
	// Ссылка для сохранения сообщения
	SaveLink string
	// Ссылка для последующего редиректа
	RedirectLink string
}

func (o *Object) forumMessageEditPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MessageEditPageName)

		messageID, err := strconv.ParseInt(r.URL.Query().Get("message-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		message, err := o.superObject.CommentDomain().Message(ctx, messageID, false)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		redirect := r.URL.Query().Get("redirect")

		data := MessageEditPageTD{
			MessageID: messageID,
			Text:      message.RawText,
			Dialects: []string{
				string(txtpDTO.PlainTextDialect),
				string(txtpDTO.MarkdownBaseDialect),
				string(txtpDTO.MarkdownExtendDialect),
			},
			SaveLink:     PathToEditMessage,
			RedirectLink: redirect,
		}

		if message.Dialect != nil {
			data.Dialect = *message.Dialect
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) forumMessageEditPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MessageEditPageName)
		err := r.ParseMultipartForm(core.MaxFileSize)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		messageID, err := strconv.ParseInt(r.FormValue("message-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		redirect := r.FormValue("redirect")
		messageText := r.FormValue("message-text")
		var messageDialect *string

		if v := r.FormValue("message-dialect"); v != "" {
			messageDialect = &v
		}

		err = o.superObject.CommentDomain().UpdateMessageText(
			ctx, userData.ID, messageID, messageText, messageDialect,
		)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		if redirect == "" {
			redirect = "/"
		}

		http.Redirect(w, r, redirect, http.StatusSeeOther)
	})
}
