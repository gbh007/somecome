package pageHandlers

import (
	"context"
	"net/http"
	commentDTO "somecome/domain/comment/dto"
	txtpDTO "somecome/domain/txtp/dto"
	userDTO "somecome/domain/user/dto"
	"somecome/domain/webserver/link"
	"somecome/pkg/logger"
	"somecome/superobject"
	"strconv"
	"strings"
)

func (o *Object) messageListPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MessageListPageName)
		pageRaw := r.URL.Query().Get("page")
		contentKey := r.URL.Query().Get("content-key")
		redirect := r.URL.Query().Get("redirect")

		userData, _ := o.superObject.AuthDomain().GetUserFromContext(ctx)

		var (
			currentPage int64 = 1
		)

		// Если указана страница то обрабатываем ее
		if pageRaw != "" && pageRaw != "last" {
			parsedPage, err := strconv.ParseInt(pageRaw, 10, 64)
			if err != nil {
				logger.DebugError(ctx, err)
			} else {
				currentPage = parsedPage
			}

		}
		_, messagePages, err := o.superObject.CommentDomain().MessageCount(ctx, contentKey)
		if err != nil {
			wsd.WritePlain(ctx, w, http.StatusBadRequest, err.Error())

			return
		}

		// Если указана последняя страница, то переходим на нее
		if pageRaw == "last" {
			currentPage = messagePages
		}

		messages, err := o.superObject.CommentDomain().Messages(ctx, contentKey, &currentPage)
		if err != nil {
			wsd.WritePlain(ctx, w, http.StatusBadRequest, err.Error())

			return
		}

		data := MessageListTD{
			Dialects: []string{
				string(txtpDTO.PlainTextDialect),
				string(txtpDTO.MarkdownBaseDialect),
				string(txtpDTO.MarkdownExtendDialect),
			},
			NewMessageLink: GetCreateMessageLink(contentKey, redirect),
		}

		data.Pages = wsd.GeneratePagination(
			currentPage, messagePages,
			GetMessagePageLinkWrapper(contentKey),
		)

		for _, rawMessage := range messages {
			msg := MessageUnit{
				Text:        rawMessage.RawText,
				Dialect:     rawMessage.Dialect,
				User:        userUnitWithoutError(o.superObject, ctx, rawMessage.CreatorID),
				Attachments: o.listMessageAttachmentUnit(ctx, rawMessage.Attachments),
				FMRKey:      rawMessage.InnerContentKey,
				Created:     rawMessage.Created,
				Updated:     rawMessage.Updated,
			}

			if userData != nil && userData.ID == rawMessage.CreatorID {
				l := GetEditMessageLink(rawMessage.ID, redirect)
				msg.EditLink = &l
			}

			data.Messages = append(data.Messages, msg)
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) listMessageAttachmentUnit(
	ctx context.Context, attachments []commentDTO.Attachment,
) (list []MessageAttachmentUnit) {
	for _, attachment := range attachments {
		info, _, err := o.superObject.FileDomain().GetFile(ctx, attachment.FileToken, false)
		if err != nil {
			logger.DebugError(ctx, err)

			return
		}

		// Только загруженные вложения отображаются в текущей версии
		if !info.Loaded {
			continue
		}

		data := MessageAttachmentUnit{}

		if info.Name != nil {
			data.Name = *info.Name
		}

		if info.Mime != nil {
			// TODO: сделать нормально, через доп. данные с файлового домена
			data.IsImage = strings.HasPrefix(*info.Mime, "image/")
		}

		data.URL = link.GetFileLink(info.Token, !data.IsImage)

		list = append(list, data)
	}

	return
}

// userUnit - заполняет данные для рендеринга пользователя по его ИД
func userUnit(
	wsd superobject.SuperObject,
	ctx context.Context, userID int64,
) (*UserUnit, error) {

	mainInfo, err := wsd.UserDomain().GetPrettyInfo(ctx, userID)
	if err != nil {

		return nil, err
	}

	info := userUnitFromModel(mainInfo)

	return info, nil
}

// userUnitWithoutError - заполняет данные для рендеринга пользователя по его ИД,
// игнорирует ошибки, и выводит их в дебаг
func userUnitWithoutError(
	wsd superobject.SuperObject,
	ctx context.Context, userID int64,
) *UserUnit {
	info, err := userUnit(wsd, ctx, userID)
	if err != nil {
		logger.DebugError(ctx, err)

		return nil
	}

	return info
}

func userUnitFromModel(raw *userDTO.PrettyInfo) *UserUnit {
	info := &UserUnit{
		ID:          raw.UserID,
		DisplayName: raw.DisplayName,
	}

	if raw.HasAvatar {
		aurl := link.GetAvatarLink(raw.UserID)
		info.AvatarURL = &aurl
	}

	return info
}
