package fileDTO

import "errors"

var (
	// Файл уже был загружен
	FileAlreadyLoaded = errors.New("file already loaded")
	// Файл еще не был загружен
	FileNotLoaded = errors.New("file not loaded")
	// Файл не принадлежит пользователю
	NotOwnerErr = errors.New("not owner")
)
