package apiHandlers

import (
	"net/http"
	"somecome/domain/webserver/core"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle("/api/file/new", core.PostWithNoOptionWrapper(wsd, o.createFileTokenHandler(wsd)))
	mux.Handle("/api/file/upload", core.PostWithNoOptionWrapper(wsd, o.fileUploadHandler(wsd)))

}
