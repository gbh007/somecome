package file

import (
	"context"
	"somecome/domain/file/database"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

// UploadFileFull - загружает файл с созданием токена при необходимости
func (domain *Domain) UploadFileFull(ctx context.Context, userID int64, file fileDTO.FileInput) (string, error) {
	if file.Token == "" {
		token, err := domain.CreateFileToken(ctx, userID)
		if err != nil {
			return "", err
		}

		file.Token = token
	} else {
		err := domain.checkFileToUpload(ctx, file.Token)
		if err != nil {
			return "", errs.WrapError(ctx, DomainError, err)
		}
	}

	// Загружаем тело файла
	size, err := domain.uploadFileBody(ctx, file.Token, file.Body)
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	// Обновляем информацию в БД
	err = domain.storage.UpdateFileAfterLoad(ctx, database.File{
		Token: file.Token,
		Name:  sqlconvert.ToNullStringE(file.Name),
		Mime:  sqlconvert.ToNullStringE(file.Mime),
		Size:  sqlconvert.ToNullInt64(&size),
	})
	if err != nil {
		return "", errs.WrapError(ctx, DomainError, err)
	}

	return file.Token, nil
}

// checkFileToUpload - проверяет возможность загрузки файла.
func (domain *Domain) checkFileToUpload(ctx context.Context, token string) error {
	// Проверяем файл на существование
	existInfo, err := domain.storage.GetFile(ctx, token)
	if err != nil {
		return err
	}

	// Проверяем файл на загруженость в хранилище
	if existInfo.Loaded {
		return fileDTO.FileAlreadyLoaded
	}

	return nil
}
