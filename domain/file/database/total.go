package database

import (
	"context"
	"somecome/pkg/errs"
)

// GetFilesByUserTotal - возвращает сумму занимаемого места и количество файлов пользователя.
func (d *Database) GetFilesByUserTotal(ctx context.Context, userID int64) (count, total int64, err error) {
	row := d.db.QueryRowContext(ctx, `SELECT COUNT(*), SUM(size) FROM file.files WHERE user_id = $1;`, userID)

	err = row.Scan(&count, &total)
	if err != nil {
		return 0, 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, total, nil
}
