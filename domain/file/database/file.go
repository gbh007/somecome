package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// File - данные о файле
type File struct {
	// Токен сессии
	Token string `db:"token"`
	// Файл загружен
	Loaded bool `db:"loaded"`
	// Ид пользователя в базе
	UserID int64 `db:"user_id"`
	// Название файла
	Name sql.NullString `db:"name"`
	// MIME тип файла
	Mime sql.NullString `db:"mime"`
	// Размер файла
	Size sql.NullInt64 `db:"size"`
	// Время создания файла
	Created time.Time `db:"created"`
	// Время последнего использования (обращения) файла
	Used sql.NullTime `db:"used"`
	// Время последнего обновления файла
	Updated sql.NullTime `db:"updated"`
}

// CreateFileToken - создает новую запись о файле
func (d *Database) CreateFileToken(ctx context.Context, token string, userID int64) error {
	_, err := d.db.ExecContext(ctx, `INSERT INTO file.files(token, user_id, created) VALUES ($1, $2, $3);`, token, userID, time.Now())
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// GetFile - возвращает файл по токену.
func (d *Database) GetFile(ctx context.Context, token string) (*File, error) {
	file := new(File)

	err := d.db.GetContext(ctx, file, `SELECT * FROM file.files WHERE token = $1 LIMIT 1;`, token)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return file, nil
}

// UpdateFileUsedTime - обновляет время использования файла.
func (d *Database) UpdateFileUsedTime(ctx context.Context, token string) error {
	_, err := d.db.ExecContext(ctx, `UPDATE file.files SET used = $1 WHERE token = $2;`, time.Now(), token)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdateFileAfterLoad - обновляет данные файла после загрузки
func (d *Database) UpdateFileAfterLoad(ctx context.Context, file File) error {
	_, err := d.db.ExecContext(
		ctx,
		`UPDATE file.files SET loaded = TRUE, name = $2, mime = $3, size = $4, updated = $5 WHERE token = $1;`,
		file.Token, file.Name, file.Mime, file.Size, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeleteFile - удаляет файл по токену.
func (d *Database) DeleteFile(ctx context.Context, token string) error {

	_, err := d.db.ExecContext(ctx, `DELETE FROM file.files WHERE token = $1;`, token)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
