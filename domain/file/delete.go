package file

import (
	"context"
	"os"
	fileDTO "somecome/domain/file/dto"
	"somecome/pkg/errs"
)

// DeleteFile - удаляет файл
func (domain *Domain) DeleteFile(ctx context.Context, token string) error {
	// Важный нюанс, файла может не быть в БД, но это не вызовет ошибку, это считается корректным поведением
	err := domain.storage.DeleteFile(ctx, token)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	err = os.Remove(domain.filepath(token))
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// DeleteUserFile - удаляет файл с проверкой соответствия пользователя
func (domain *Domain) DeleteUserFile(ctx context.Context, token string, userID int64) error {
	fileInfo, _, err := domain.GetFile(ctx, token, false)
	if err != nil {
		return err
	}

	if fileInfo.UserID != userID {
		return errs.WrapError(ctx, DomainError, fileDTO.NotOwnerErr)
	}

	err = domain.DeleteFile(ctx, token)
	if err != nil {
		return err
	}

	return nil
}
