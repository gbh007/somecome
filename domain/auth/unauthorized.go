package auth

import (
	"context"
	"somecome/domain/auth/database"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

// ListPoU - данные для рендеринга прав неавторизованных пользователей
func (domain *Domain) ListPoU(ctx context.Context) ([]*authDTO.DomainWithPermissions, error) {
	permissionOfUnauthorized, err := domain.storage.SelectPermissionOfUnauthorized(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	pouTmp := make(map[int64]*database.PermissionsOfUnauthorized)

	for _, permission := range permissionOfUnauthorized {
		pouTmp[permission.PermissionID] = permission
	}

	domains, err := domain.storage.SelectDomains(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	permissions, err := domain.storage.SelectPermissions(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*authDTO.DomainWithPermissions, 0, len(domains))
	for _, dom := range domains {
		permissionList := make([]*authDTO.PermissionStatus, 0)

		for _, perm := range permissions {
			// Только для совпадения по домену
			if perm.DomainCode != dom.Code {
				continue
			}

			permissionStatus := &authDTO.PermissionStatus{
				ID:          perm.ID,
				Code:        perm.Code,
				Name:        perm.Name,
				Description: sqlconvert.FromNullString(perm.Description),
			}

			// Если есть настройка то добавляем ее
			if pou, ok := pouTmp[perm.ID]; ok {
				lv := pou.Level
				permissionStatus.Level = &lv
				permissionStatus.Created = &pou.Created
				permissionStatus.Updated = sqlconvert.FromNullTime(pou.Updated)
			}

			permissionList = append(permissionList, permissionStatus)
		}

		result = append(result, &authDTO.DomainWithPermissions{
			Code:        dom.Code,
			Name:        dom.Name,
			Description: sqlconvert.FromNullString(dom.Description),
			Permissions: permissionList,
		})
	}

	return result, nil
}

// SetPoU - устанавливаем права для не авторизированных пользователей,
// важно: не фильтрует дубли для новых прав, и они будут вызывать ошибку
func (domain *Domain) SetPoU(ctx context.Context, newPermissions []authDTO.PermissionInput) error {
	oldPermission, err := domain.storage.SelectPermissionOfUnauthorized(ctx)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	oldPermissionMap := make(map[int64]*database.PermissionsOfUnauthorized)

	for _, permission := range oldPermission {
		oldPermissionMap[permission.PermissionID] = permission
	}

	// Обновляем/добавляем права
	for _, permission := range newPermissions {
		oldValue, exists := oldPermissionMap[permission.PermissionID]

		if exists {
			delete(oldPermissionMap, permission.PermissionID)

			// Если уровни совпали, то обновление не требуется
			if oldValue.Level == int16(permission.Level) {
				continue
			}

			err = domain.storage.UpdatePermissionOfUnauthorized(ctx, permission.PermissionID, int16(permission.Level))
		} else {
			err = domain.storage.InsertPermissionOfUnauthorized(ctx, permission.PermissionID, int16(permission.Level))
		}

		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	}

	// Удаляем старые, если остались
	for permissionID := range oldPermissionMap {
		err = domain.storage.DeletePermissionOfUnauthorized(ctx, permissionID)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	}

	return nil
}
