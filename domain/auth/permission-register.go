package auth

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/pkg/sqlconvert"
)

// RegisterPermissions - регистрирует права
func (domain *Domain) RegisterPermissions(ctx context.Context, domains ...authDTO.RegisterDomainInput) error {
	for _, dmn := range domains {
		err := domain.registerPermissionDomain(ctx, dmn)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}

		for _, p := range dmn.Permissions {
			err = domain.registerPermission(ctx, dmn.Code, p)
			if err != nil {
				return errs.WrapError(ctx, DomainError, err)
			}
		}
	}

	return nil
}

// registerPermissionDomain - регистрирует домены для прав
func (domain *Domain) registerPermissionDomain(ctx context.Context, dmn authDTO.RegisterDomainInput) error {
	newDescription := sqlconvert.ToNullStringE(dmn.Description)
	domainInfo, err := domain.storage.SelectDomain(ctx, dmn.Code)

	if errors.Is(err, sql.ErrNoRows) {
		insertErr := domain.storage.InsertDomain(ctx, dmn.Code, dmn.Name, newDescription)
		if insertErr != nil {
			return insertErr
		}

		return nil
	}

	if err != nil {
		return err
	}

	hasChanged := domainInfo.Name != dmn.Name

	if newDescription.Valid != domainInfo.Description.Valid ||
		newDescription.String != domainInfo.Description.String {
		hasChanged = true
	}

	// Нет изменений, выходим
	if !hasChanged {
		return nil
	}

	err = domain.storage.UpdateDomain(ctx, dmn.Code, dmn.Name, newDescription)
	if err != nil {
		return err
	}

	return nil
}

// registerPermission - регистрирует право доступа
func (domain *Domain) registerPermission(ctx context.Context, domainCode string, permission authDTO.DomainPermission) error {
	newDescription := sqlconvert.ToNullStringE(permission.Description)
	permissionInfo, err := domain.storage.SelectPermissionByCode(ctx, permission.Code)

	if errors.Is(err, sql.ErrNoRows) {
		insertErr := domain.storage.InsertPermission(ctx, domainCode, permission.Code, permission.Name, newDescription)
		if insertErr != nil {
			return insertErr
		}

		return nil
	}

	if err != nil {
		return err
	}

	hasChanged := permissionInfo.Name != permission.Name

	if domainCode != permissionInfo.DomainCode {
		logger.Warning(ctx, fmt.Sprintf(
			"warning permission domain changed: %s -> %s",
			permissionInfo.DomainCode,
			domainCode,
		))
		hasChanged = true
	}

	if newDescription.Valid != permissionInfo.Description.Valid ||
		newDescription.String != permissionInfo.Description.String {
		hasChanged = true
	}

	// Нет изменений, выходим
	if !hasChanged {
		return nil
	}

	err = domain.storage.UpdatePermission(ctx, domainCode, permission.Code, permission.Name, newDescription)
	if err != nil {
		return err
	}

	return nil
}
