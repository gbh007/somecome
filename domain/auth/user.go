package auth

import (
	"context"
	"database/sql"
	"errors"
	"somecome/domain/auth/database"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/auth/logic"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"strings"
)

func userFromDB(raw *database.User) *authDTO.User {
	return &authDTO.User{
		ID:      raw.ID,
		Login:   raw.Login,
		Created: raw.Created,
	}
}

// CreateUser - создает нового пользователя
func (domain *Domain) CreateUser(ctx context.Context, login, password string) (int64, error) {
	autoGroups, err := domain.storage.SelectAutoAppendGroups(ctx)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	salt := logic.RandomSHA256String()
	login = strings.ToLower(login)

	id, err := domain.storage.CreateUser(ctx, &database.User{
		Login:    login,
		Password: logic.SaltPassword(password, salt),
		Salt:     salt,
	})
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	for _, group := range autoGroups {
		err = domain.storage.InsertGroupUser(ctx, group.ID, id)
		if err != nil {
			return 0, errs.WrapError(ctx, DomainError, err)
		}
	}

	return id, nil
}

// GetUser - возвращает данные пользователя по ИД
func (domain *Domain) GetUser(ctx context.Context, id int64) (*authDTO.User, error) {
	user, err := domain.storage.GetUserByID(ctx, id)

	if errors.Is(err, sql.ErrNoRows) {
		return nil, errs.WrapError(ctx, DomainError, authDTO.UserNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return userFromDB(user), nil
}

// GetUserBySession - возвращает данные пользователя по токену сессии
func (domain *Domain) GetUserBySession(ctx context.Context, token string) (*authDTO.User, error) {
	session, err := domain.storage.GetSessionByToken(ctx, token)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, errs.WrapError(ctx, DomainError, authDTO.SessionNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	if session.IsClosed {
		return nil, errs.WrapError(ctx, DomainError, authDTO.SessionIsClosedErr)
	}

	user, err := domain.storage.GetUserByID(ctx, session.UserID)

	if errors.Is(err, sql.ErrNoRows) {
		return nil, errs.WrapError(ctx, DomainError, authDTO.UserNotFoundErr)
	}

	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	logger.IfErr(ctx, domain.storage.UpdateSessionUsedTime(ctx, token))

	return userFromDB(user), nil
}

// AllUsers - возвращает данные всех пользователей
func (domain *Domain) AllUsers(ctx context.Context) ([]*authDTO.User, error) {
	users, err := domain.storage.GetUsers(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*authDTO.User, 0, len(users))
	for _, user := range users {
		result = append(result, userFromDB(user))
	}

	return result, nil
}
