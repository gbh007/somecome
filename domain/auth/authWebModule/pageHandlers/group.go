package pageHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
	"time"
)

// GroupUnit - группа доступов пользователей
type GroupUnit struct {
	// ИД группы
	ID int64
	// Название группы
	Name string
	// Описание группы
	Description *string
	// Признак того что пользователя после регистрации необходимо автоматически добавить в группу
	AutoAppend bool
	// Время создания группы
	Created time.Time
	// Количество пользователей в группе
	UserCount int64
	// Количество привилегий у группы
	PermissionCount int64
	// Ссылка на редактирование
	EditorLink string
}

// GroupListTD - данные для рендеринга списка групп доступа
type GroupListTD struct {
	// Список групп
	Groups []GroupUnit
	// Ссылка на создание новой
	CreateNewLink string
}

// GroupEditorTD - данные для рендеринга редактора группы доступа
type GroupEditorTD struct {
	// Данные группы
	Group GroupUnit
	// Ссылка на сохранение
	SaveLink string
	// Ссылка на редактор прав
	PermissionsEditorLink string
	// Ссылка на редактор пользователей
	UsersEditorLink string
	// Новая группа
	IsNew bool
}

func (o *Object) groupListGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, AuthGroupListPageName)

		groups, err := o.superObject.AuthDomain().ListGroups(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := GroupListTD{
			CreateNewLink: GetAuthGroupEditorLink(nil),
		}

		for _, group := range groups {
			data.Groups = append(data.Groups, GroupUnit{
				ID:              group.ID,
				Name:            group.Name,
				Description:     group.Description,
				AutoAppend:      group.AutoAppend,
				Created:         group.Created,
				UserCount:       group.UserCount,
				PermissionCount: group.PermissionCount,
				EditorLink:      GetAuthGroupEditorLink(&group.ID),
			})
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) groupEditorGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, AuthGroupInfoEditorPageName)

		data := GroupEditorTD{
			SaveLink: PathToAuthGroupEditor,
		}

		rawID := r.URL.Query().Get("id")

		if rawID != "" && rawID != "new" {
			id, err := strconv.ParseInt(rawID, 10, 64)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}

			group, err := o.superObject.AuthDomain().GetGroup(ctx, id)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}

			data.Group = GroupUnit{
				ID:              group.ID,
				Name:            group.Name,
				Description:     group.Description,
				AutoAppend:      group.AutoAppend,
				Created:         group.Created,
				UserCount:       group.UserCount,
				PermissionCount: group.PermissionCount,
			}
			data.PermissionsEditorLink = GetAuthGroupPermissionEditorLink(id)
			data.UsersEditorLink = GetAuthGroupUsersEditorLink(id)
		} else {
			data.Group = GroupUnit{
				Created: time.Now(),
			}
			data.IsNew = true
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) groupEditorPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		err := r.ParseForm()
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		rawID := r.FormValue("id")
		actionType := r.FormValue("at")
		name := r.FormValue("name")
		description := r.FormValue("description")
		autoAppend := r.Form.Has("auto-append")
		isNew := rawID == "" || rawID == "new"

		var id int64

		if !isNew {
			id, err = strconv.ParseInt(rawID, 10, 64)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		}

		group := authDTO.Group{
			ID:         id,
			Name:       name,
			AutoAppend: autoAppend,
		}

		if description != "" {
			group.Description = &description
		}

		if isNew {
			id, err = o.superObject.AuthDomain().CreateGroup(ctx, group)
		} else {
			err = o.superObject.AuthDomain().UpdateGroup(ctx, group)
		}

		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		switch actionType {
		case "save":
			http.Redirect(w, r, GetAuthGroupEditorLink(&id), http.StatusSeeOther)
		default:
			http.Redirect(w, r, PathToAuthGroupList, http.StatusSeeOther)
		}
	})
}
