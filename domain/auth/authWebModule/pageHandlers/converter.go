package pageHandlers

import (
	"fmt"
	"net/url"
	authDTO "somecome/domain/auth/dto"
	"strconv"
)

func fromLevels(raw []*authDTO.LevelInfo, current *int16) []LevelInfoUnit {
	out := make([]LevelInfoUnit, 0, len(raw))

	for _, level := range raw {
		selected := current != nil && level.Level == *current
		out = append(out, LevelInfoUnit{
			Level:    int16(level.Level),
			Name:     level.Name,
			Selected: selected,
		})
	}

	return out
}

func fromPermissions(
	raw []*authDTO.PermissionStatus,
	rawLevels []*authDTO.LevelInfo,
) []PermissionStatusUnit {
	out := make([]PermissionStatusUnit, 0, len(raw))

	for _, status := range raw {
		var lv *int16

		if status.Level != nil {
			tmp := int16(*status.Level)
			lv = &tmp
		}

		out = append(out, PermissionStatusUnit{
			ID:          status.ID,
			Code:        status.Code,
			Name:        status.Name,
			Description: status.Description,
			Level:       lv,
			Created:     status.Created,
			Updated:     status.Updated,
			Levels:      fromLevels(rawLevels, status.Level),
		})
	}

	return out
}

func fromDomains(
	raw []*authDTO.DomainWithPermissions,
	rawLevels []*authDTO.LevelInfo,
) []DomainWithPermissionsUnit {
	out := make([]DomainWithPermissionsUnit, 0, len(raw))

	for _, domain := range raw {
		out = append(out, DomainWithPermissionsUnit{
			Code:        domain.Code,
			Name:        domain.Name,
			Description: domain.Description,
			Permissions: fromPermissions(domain.Permissions, rawLevels),
		})
	}

	return out
}

func toPermissionInput(values url.Values) ([]authDTO.PermissionInput, error) {

	idsRaw := values["permission-id"]
	levelsRaw := values["level"]

	if len(idsRaw) != len(levelsRaw) {
		return nil, fmt.Errorf("not equal input data lengths")
	}

	out := make([]authDTO.PermissionInput, 0, len(idsRaw))

	for index := range idsRaw {
		if levelsRaw[index] == "" {
			continue
		}

		level, err := strconv.ParseInt(levelsRaw[index], 10, 16)
		if err != nil {
			return nil, fmt.Errorf("can not parse level: %w", err)
		}

		id, err := strconv.ParseInt(idsRaw[index], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("can not parse id: %w", err)
		}

		out = append(out, authDTO.PermissionInput{
			Level:        int16(level),
			PermissionID: id,
		})
	}

	return out, nil
}

func toUsersInput(values url.Values, key string) ([]int64, error) {
	result := make([]int64, 0)

	for _, value := range values[key] {
		id, err := strconv.ParseInt(value, 10, 16)
		if err != nil {
			return nil, fmt.Errorf("can not parse id: %w", err)
		}

		result = append(result, id)
	}

	return result, nil
}
