package pageHandlers

import (
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/core"
	"somecome/domain/webserver/link"
	"somecome/pkg/errs"
	"somecome/superobject"
)

type loginPageTD struct {
	// Ссылка на авторизацию
	LoginLink string
	// Ссылка на регистрацию
	RegistrationLink string
}

func (o *Object) loginPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tdata := wsd.TemplateData(r.Context(), LoginPageTemplateName)
		tdata.SetData(loginPageTD{
			LoginLink:        link.PathToLogin,
			RegistrationLink: PathToRegistration,
		})

		wsd.WriteTemplate(r.Context(), w, http.StatusOK, tdata)
	})
}

func (o *Object) loginPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, LoginPageTemplateName)
		tdata.SetData(loginPageTD{
			LoginLink:        link.PathToLogin,
			RegistrationLink: PathToRegistration,
		})

		login := r.FormValue("login")
		password := r.FormValue("password")

		token, err := o.superObject.AuthDomain().CreateSession(ctx, login, password)

		if errors.Is(err, authDTO.LoginOrPasswordIncorrectErr) {
			err = errs.AddText(err, "не корректный логин/пароль")
		}

		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     core.SessionCookieName,
			Value:    token,
			HttpOnly: true,
			Path:     "/",
		})

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})
}
