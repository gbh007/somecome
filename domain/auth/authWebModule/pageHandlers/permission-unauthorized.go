package pageHandlers

import (
	"net/http"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

func (o *Object) permissionOfUnauthorizedGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, AuthPermissionEditorPageName)

		permissions, err := o.superObject.AuthDomain().ListPoU(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		levels, err := o.superObject.AuthDomain().ListLevels(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		data := PermissionEditorTD{
			Domains:  fromDomains(permissions, levels),
			SaveLink: PathToControlUnauthorized,
			Name:     "Права не авторизованных",
		}
		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) permissionOfUnauthorizedPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		err := r.ParseForm()
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		actionType := r.FormValue("at")

		permissions, err := toPermissionInput(r.Form)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		err = o.superObject.AuthDomain().SetPoU(ctx, permissions)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		switch actionType {
		case "save":
			http.Redirect(w, r, PathToControlUnauthorized, http.StatusSeeOther)
		default:
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	})
}
