package pageHandlers

import (
	"net/url"
	"somecome/domain/webserver/link"
	"strconv"
)

const moduleTemplateName = "module:auth:page"

// Коды страниц для рендеринга шаблонов
var (
	AuthPermissionEditorPageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:permission-editor"}
	AuthGroupInfoEditorPageName  = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:group-info-editor"}
	AuthGroupListPageName        = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:group-list"}
	AuthGroupUsersEditorPageName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:group-user-editor"}
	LoginPageTemplateName        = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:login"}
	RegistrationPageTemplateName = link.PageTemplateName{TemplateName: moduleTemplateName, PageName: "module:auth:page:registration"}
)

// Ендпоинты веб сервера
const (
	PathToRegistration               = "/pages/auth/registration"
	PathToControlUnauthorized        = "/pages/auth/control/unauthorized"
	PathToAuthGroupEditor            = "/pages/auth/group"
	PathToAuthGroupList              = "/pages/auth/group/list"
	PathToAuthGroupPermissionsEditor = "/pages/auth/group/permissions"
	PathToAuthGroupUsersEditor       = "/pages/auth/group/users"
)

// GetAuthGroupEditorLink - генерирует ссылку на редактор группы доступа
func GetAuthGroupEditorLink(groupID *int64) string {
	link := url.URL{
		Path: PathToAuthGroupEditor,
	}

	args := url.Values{}

	if groupID != nil {
		args.Set("id", strconv.FormatInt(*groupID, 10))
	} else {
		args.Set("id", "new")
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetAuthGroupPermissionEditorLink - генерирует ссылку на редактор прав группы доступа
func GetAuthGroupPermissionEditorLink(groupID int64) string {
	link := url.URL{
		Path: PathToAuthGroupPermissionsEditor,
	}

	args := url.Values{}
	args.Set("id", strconv.FormatInt(groupID, 10))
	link.RawQuery = args.Encode()

	return link.String()
}

// GetAuthGroupUsersEditorLink - генерирует ссылку на редактор пользователей группы доступа
func GetAuthGroupUsersEditorLink(groupID int64) string {
	link := url.URL{
		Path: PathToAuthGroupUsersEditor,
	}

	args := url.Values{}
	args.Set("id", strconv.FormatInt(groupID, 10))
	link.RawQuery = args.Encode()

	return link.String()
}
