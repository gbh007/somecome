package pageHandlers

import (
	"net/http"
	"somecome/domain/auth/authShared"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(link.PathToLogin, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.loginPageGet(wsd),
			http.MethodPost: o.loginPagePost(wsd),
		},
	))

	mux.Handle(PathToRegistration, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.registrationPageGet(wsd), authShared.AuthRegistrationPC, authDTO.LevelFullAccess),
			http.MethodPost: wsd.PermissionTDHandler(o.registrationPagePost(wsd), authShared.AuthRegistrationPC, authDTO.LevelFullAccess),
		},
	))

	mux.Handle(link.PathToLogout, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.logoutPageGetPost(wsd),
			http.MethodPost: o.logoutPageGetPost(wsd),
		},
	))

	mux.Handle(PathToControlUnauthorized, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.permissionOfUnauthorizedGetHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadOnly),
			http.MethodPost: wsd.PermissionTDHandler(o.permissionOfUnauthorizedPostHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadWrite),
		},
	))

	mux.Handle(PathToAuthGroupList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.groupListGetHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadOnly),
		},
	))

	mux.Handle(PathToAuthGroupEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.groupEditorGetHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadOnly),
			http.MethodPost: wsd.PermissionTDHandler(o.groupEditorPostHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadWrite),
		},
	))

	mux.Handle(PathToAuthGroupPermissionsEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.groupPermissionEditorGetHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadOnly),
			http.MethodPost: wsd.PermissionTDHandler(o.groupPermissionEditorPostHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadWrite),
		},
	))

	mux.Handle(PathToAuthGroupUsersEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  wsd.PermissionTDHandler(o.groupUsersEditorGetHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadOnly),
			http.MethodPost: wsd.PermissionTDHandler(o.groupUsersEditorPostHandler(wsd), authShared.AuthPermissionControlPC, authDTO.LevelReadWrite),
		},
	))
}
