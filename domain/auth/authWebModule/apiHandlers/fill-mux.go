package apiHandlers

import (
	"net/http"
	"somecome/domain/auth/authShared"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/core"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle("/api/auth/login", core.PostWithNoOptionWrapper(wsd, o.loginHandler(wsd)))
	mux.Handle("/api/auth/login-jwt", core.PostWithNoOptionWrapper(wsd, o.createJWTHandler(wsd)))
	mux.Handle("/api/auth/register", core.PostWithNoOptionWrapper(wsd, wsd.PermissionJSONHandler(o.registerHandler(wsd), authShared.AuthRegistrationPC, authDTO.LevelFullAccess)))
}
