package auth

import (
	"context"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/errs"
)

// ListLevels - данные списка уровней доступа
func (domain *Domain) ListLevels(ctx context.Context) ([]*authDTO.LevelInfo, error) {
	levels, err := domain.storage.SelectPermissionLevels(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*authDTO.LevelInfo, 0, len(levels))
	for _, lv := range levels {
		result = append(result, &authDTO.LevelInfo{
			Level: lv.Level,
			Name:  lv.Name,
		})
	}

	return result, nil
}
