package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// GroupPermission - доступ для группы
type GroupPermission struct {
	// ИД группы
	GroupID int64 `db:"group_id"`
	// ИД доступа
	PermissionID int64 `db:"permission_id"`
	// Уровень доступа
	Level int16 `db:"level"`
	// Время создания доступа для группы
	Created time.Time `db:"created"`
	// Время обновления доступа для группы
	Updated sql.NullTime `db:"updated"`
}

// SelectCountPermissionInGroup - получает количество прав доступа группы
func (d *Database) SelectCountPermissionInGroup(ctx context.Context, groupID int64) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT COUNT(*) FROM auth.group_permissions WHERE group_id = $1;`, groupID)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}

// SelectGroupPermissions - получает список уровней доступа группы
func (d *Database) SelectGroupPermissions(ctx context.Context, groupID int64) ([]*GroupPermission, error) {
	list := make([]*GroupPermission, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.group_permissions WHERE group_id = $1 ORDER BY permission_id;`, groupID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// InsertGroupPermission - добавляет новую привилегию группы
func (d *Database) InsertGroupPermission(ctx context.Context, groupID int64, permissionID int64, level int16) error {
	_, err := d.db.ExecContext(ctx,
		`INSERT INTO auth.group_permissions (group_id, permission_id, level, created) VALUES ($1, $2, $3, $4);`,
		groupID, permissionID, level, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdateGroupPermission - обновляет привилегию группы
func (d *Database) UpdateGroupPermission(ctx context.Context, groupID int64, permissionID int64, level int16) error {
	_, err := d.db.ExecContext(ctx,
		`UPDATE auth.group_permissions SET level = $3, updated = $4 WHERE group_id = $1 AND permission_id = $2;`,
		groupID, permissionID, level, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeleteGroupPermission - удаляет привилегию группы
func (d *Database) DeleteGroupPermission(ctx context.Context, groupID int64, permissionID int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM auth.group_permissions WHERE group_id = $1 AND permission_id = $2;`, groupID, permissionID)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
