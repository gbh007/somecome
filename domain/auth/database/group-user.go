package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// GroupUser - пользователь группы
type GroupUser struct {
	// ИД группы
	GroupID int64 `db:"group_id"`
	// ИД пользователя
	UserID int64 `db:"user_id"`
	// Время привязки пользователя к группе
	Created time.Time `db:"created"`
}

// SelectCountUserInGroup - получает количество участников группы
func (d *Database) SelectCountUserInGroup(ctx context.Context, groupID int64) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT COUNT(*) FROM auth.group_users WHERE group_id = $1;`, groupID)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}

// SelectGroupUsers - получает список пользователей группы доступа
func (d *Database) SelectGroupUsers(ctx context.Context, groupID int64) ([]*GroupUser, error) {
	list := make([]*GroupUser, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.group_users WHERE group_id = $1 ORDER BY user_id;`, groupID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// InsertGroupUser - добавляет пользователя в группу
func (d *Database) InsertGroupUser(ctx context.Context, groupID int64, userID int64) error {
	_, err := d.db.ExecContext(ctx,
		`INSERT INTO auth.group_users (group_id, user_id, created) VALUES ($1, $2, $3);`,
		groupID, userID, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// DeleteGroupUser - удаляет пользователя из группы
func (d *Database) DeleteGroupUser(ctx context.Context, groupID int64, userID int64) error {
	_, err := d.db.ExecContext(ctx,
		`DELETE FROM auth.group_users WHERE group_id = $1 AND user_id = $2;`,
		groupID, userID,
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
