package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Permission - право доступа
type Permission struct {
	// ИД доступа
	ID int64 `db:"id"`
	// Код домена
	DomainCode string `db:"domain_code"`
	// Код доступа
	Code string `db:"code"`
	// Название доступа
	Name string `db:"name"`
	// Описание доступа
	Description sql.NullString `db:"description"`
	// Время создания доступа
	Created time.Time `db:"created"`
	// Время обновления доступа
	Updated sql.NullTime `db:"updated"`
}

// SelectPermissions - получает список прав доступа
func (d *Database) SelectPermissions(ctx context.Context) ([]*Permission, error) {
	list := make([]*Permission, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM auth.permissions ORDER BY code;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectPermissionByCode - получает право доступа по коду
func (d *Database) SelectPermissionByCode(ctx context.Context, code string) (*Permission, error) {
	item := new(Permission)

	err := d.db.GetContext(ctx, item, `SELECT * FROM auth.permissions WHERE code = $1 LIMIT 1;`, code)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return item, nil
}

// InsertPermission - добавляет право доступа
func (d *Database) InsertPermission(ctx context.Context, domain, code, name string, description sql.NullString) error {
	_, err := d.db.ExecContext(ctx,
		`INSERT INTO auth.permissions(domain_code, code, "name", "description", "created") VALUES ($1, $2, $3, $4, $5);`,
		domain, code, name, description, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// UpdatePermission - обновляет право доступа
func (d *Database) UpdatePermission(ctx context.Context, domain, code, name string, description sql.NullString) error {
	_, err := d.db.ExecContext(ctx,
		`UPDATE auth.permissions SET domain_code = $2, "name" = $3, "description" = $4, "updated" = $5 WHERE code = $1;`,
		code, domain, name, description, time.Now(),
	)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
