package authDTO

import "time"

// Domain - домен для группировки доступов
type DomainWithPermissions struct {
	// Код домена
	Code string
	// Название домена
	Name string
	// Описание домена
	Description *string

	// Права в домене
	Permissions []*PermissionStatus
}

// PermissionStatus - право доступа у пользователя/группы
type PermissionStatus struct {
	// ИД доступа
	ID int64
	// Код доступа
	Code string
	// Название доступа
	Name string
	// Описание доступа
	Description *string

	// Уровень доступа
	Level *int16

	// Время создания доступа для пользователя/группы
	Created *time.Time
	// Время обновления доступа для пользователя/группы
	Updated *time.Time
}

// PermissionInput - данные для обновления прав
type PermissionInput struct {
	// Уровень доступа
	Level int16
	// ИД права доступа
	PermissionID int64
}
