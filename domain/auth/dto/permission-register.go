package authDTO

// Коды доступов
//
// TODO: перенести в новый домен тестирования
const TestingAlphaAccess = "testing-alpha-access"

// RegisterDomainInput - входные данные для регистрации домен для группировки доступов
type RegisterDomainInput struct {
	// Код домена
	Code string
	// Название домена
	Name string
	// Описание домена
	Description string
	// Права доступа
	Permissions []DomainPermission
}

// DomainPermission - право доступа для регистрации
type DomainPermission struct {
	// Код доступа
	Code string
	// Название доступа
	Name string
	// Описание доступа
	Description string
}
