package auth

import (
	"context"
	"crypto/ed25519"
	"encoding/base64"
	"somecome/config"
	"somecome/domain/auth/authShared"
	"somecome/domain/auth/authWebModule"
	"somecome/domain/auth/database"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/auth/jwt"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с аутентификацией
type Domain struct {
	storage    *database.Database
	publicKey  ed25519.PublicKey
	privateKey ed25519.PrivateKey
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *authWebModule.Module
}

func encodeKey(key []byte) string {
	return base64.RawStdEncoding.EncodeToString(key)
}

func decodeKey(ctx context.Context, key string) ([]byte, error) {
	decodedSegment, err := base64.RawStdEncoding.DecodeString(key)
	if err != nil {
		return nil, err
	}

	return decodedSegment, nil
}

// Name - название домена
func (_ *Domain) Name() string { return "auth" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = authWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	err := domain.superObject.AuthDomain().RegisterPermissions(ctx, authDTO.RegisterDomainInput{
		Code:        domain.Name(),
		Name:        "Контроль доступа",
		Description: "Домен прав для конфигурации авторизации",
		Permissions: []authDTO.DomainPermission{
			{
				Code:        authShared.AuthPermissionControlPC,
				Name:        "Управление правами пользователей",
				Description: "Выдача прав пользователю, управление группами прав",
			},
			{
				Code:        authShared.AuthRegistrationPC,
				Name:        "Регистрация нового пользователя",
				Description: "Управление регистрацией нового пользователя",
			},
			{ // TODO: перенести в новый домен тестирования
				Code:        authDTO.TestingAlphaAccess,
				Name:        "Доступ к альфа тестированию",
				Description: "Позволяет пользователю использовать функционал на альфа тестировании",
			},
		},
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	jwtConfig := cfg.JWT

	var (
		err        error
		publicKey  ed25519.PublicKey
		privateKey ed25519.PrivateKey
	)

	if jwtConfig.PublicKey != "" && jwtConfig.PrivateKey != "" {
		publicKey, err = decodeKey(ctx, jwtConfig.PublicKey)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}

		privateKey, err = decodeKey(ctx, jwtConfig.PrivateKey)
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	} else {
		publicKey, privateKey, err = jwt.NewKeyPair()
		if err != nil {
			return errs.WrapError(ctx, DomainError, err)
		}
	}

	logger.Debug(ctx, "jwt - publicKey >>>", encodeKey(publicKey))
	logger.Debug(ctx, "jwt - privateKey >>>", encodeKey(privateKey))

	domain.privateKey = privateKey
	domain.publicKey = publicKey
	domain.storage = database.New(db)

	return nil
}
