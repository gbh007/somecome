package auth

import (
	"context"
	"somecome/domain/auth/database"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

// ListGroups - возвращает список групп доступа
func (domain *Domain) ListGroups(ctx context.Context) ([]*authDTO.Group, error) {
	rawList, err := domain.storage.SelectGroups(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	result := make([]*authDTO.Group, 0, len(rawList))

	for _, groupInfo := range rawList {
		group, err := domain.convertGroup(ctx, groupInfo)
		if err != nil {
			return nil, errs.WrapError(ctx, DomainError, err)
		}

		result = append(result, group)
	}

	return result, nil
}

// GetGroup - возвращает группу доступа по ИД
func (domain *Domain) GetGroup(ctx context.Context, id int64) (*authDTO.Group, error) {
	raw, err := domain.storage.SelectGroup(ctx, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	group, err := domain.convertGroup(ctx, raw)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return group, nil
}

// UpdateGroup - обновляет данные группы
func (domain *Domain) UpdateGroup(ctx context.Context, group authDTO.Group) error {
	err := domain.storage.UpdateGroup(
		ctx,
		group.ID,
		group.Name,
		sqlconvert.ToNullString(group.Description),
		group.AutoAppend,
	)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// CreateGroup - создает данные группы
func (domain *Domain) CreateGroup(ctx context.Context, group authDTO.Group) (int64, error) {
	id, err := domain.storage.InsertGroup(
		ctx,
		group.Name,
		sqlconvert.ToNullString(group.Description),
		group.AutoAppend,
	)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}

// convertGroup - преобразует данные группы из БД
func (domain *Domain) convertGroup(ctx context.Context, raw *database.Group) (*authDTO.Group, error) {
	userCount, err := domain.storage.SelectCountUserInGroup(ctx, raw.ID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	permissionCount, err := domain.storage.SelectCountPermissionInGroup(ctx, raw.ID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return &authDTO.Group{
		ID:              raw.ID,
		Name:            raw.Name,
		Description:     sqlconvert.FromNullString(raw.Description),
		AutoAppend:      raw.AutoAppend,
		Created:         raw.Created,
		UserCount:       userCount,
		PermissionCount: permissionCount,
	}, nil
}
