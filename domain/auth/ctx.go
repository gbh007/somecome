package auth

import (
	"context"
	authDTO "somecome/domain/auth/dto"
	"somecome/pkg/ctxench"
	"somecome/pkg/errs"
)

var userContextKey = &ctxench.CtxKey{Some: "userContextKey"}

// CtxWithUser - вносит пользователя в контекст
func (_ *Domain) CtxWithUser(ctx context.Context, user *authDTO.User) context.Context {
	return context.WithValue(ctx, userContextKey, user)
}

// GetUserFromContext - получает пользователя из контекста
func (_ *Domain) GetUserFromContext(ctx context.Context) (*authDTO.User, error) {
	user, ok := ctx.Value(userContextKey).(*authDTO.User)
	if !ok {
		return nil, errs.WrapError(ctx, DomainError, authDTO.UserNotFoundErr)
	}

	return user, nil
}
