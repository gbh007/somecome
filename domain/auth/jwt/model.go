package jwt

import (
	"time"
)

const (
	tokenPartsCount = 3
	jwtAlgorithm    = "EdDSA"
	jwtType         = "JWT"
)

type TokenHeader struct {
	Algorithm string `json:"alg"`
	Type      string `json:"typ"`
}

type TokenPayload struct {
	AccountID int64     `json:"account_id"`
	Created   time.Time `json:"created"`
}

type Token struct {
	Header  *TokenHeader
	Payload *TokenPayload
}
