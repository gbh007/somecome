package jwt

import (
	"context"
	"crypto/ed25519"
	"encoding/json"
	"fmt"
	"somecome/pkg/errs"
	"strings"
)

func Decode(ctx context.Context, encodedToken string, publicKey ed25519.PublicKey) (*Token, error) {
	parts := strings.Split(encodedToken, ".")
	if len(parts) != tokenPartsCount {
		return nil, errs.WrapErrorf(ctx, JWTErrorGroup, "token: len(parts) != %d", tokenPartsCount)
	}

	decoded, err := decodeJWTSegments(parts)
	if err != nil {
		return nil, errs.WrapError(ctx, JWTErrorGroup, err)
	}

	// Ровно 3 части, проверка выполняется выше
	rawHeader, rawPayload, rawSignature := decoded[0], decoded[1], decoded[2]

	header := new(TokenHeader)
	payload := new(TokenPayload)

	err = json.Unmarshal(rawHeader, &header)
	if err != nil {
		return nil, errs.WrapError(ctx, JWTErrorGroup, fmt.Errorf(`header: unmarshal error: %w`, err))
	}

	if header.Type != jwtType {
		return nil, errs.WrapError(ctx, JWTErrorGroup, fmt.Errorf(`header: unsupported type %s`, header.Type))
	}
	if header.Algorithm != jwtAlgorithm {
		return nil, errs.WrapError(ctx, JWTErrorGroup, fmt.Errorf(`header: unsupported algorithm %s`, header.Algorithm))
	}

	err = json.Unmarshal(rawPayload, &payload)
	if err != nil {
		return nil, errs.WrapError(ctx, JWTErrorGroup, fmt.Errorf(`payload: unmarshal error: %w`, err))
	}

	if !checkTokenSign(
		jwtPartWithoutSignatureFromRaw(rawHeader, rawPayload),
		rawSignature,
		publicKey,
	) {
		return nil, errs.WrapErrorf(ctx, JWTErrorGroup, "token: invalid sign")
	}

	return &Token{
		Header:  header,
		Payload: payload,
	}, nil
}

// Encode - создает и подписывает JWT из нагрузки
func Encode(ctx context.Context, payload TokenPayload, privateKey ed25519.PrivateKey) (string, error) {
	rawHeader, err := json.Marshal(TokenHeader{
		Algorithm: jwtAlgorithm,
		Type:      jwtType,
	})
	if err != nil {
		return "", errs.WrapError(ctx, JWTErrorGroup, err)
	}

	rawPayload, err := json.Marshal(payload)
	if err != nil {
		return "", errs.WrapError(ctx, JWTErrorGroup, err)
	}

	tokenWithoutSign := jwtPartWithoutSignatureFromRaw(rawHeader, rawPayload)

	signature, err := signToken(privateKey, tokenWithoutSign)
	if err != nil {
		return "", errs.WrapError(ctx, JWTErrorGroup, err)
	}

	return string(tokenWithoutSign) + "." + encodeJWTSegment(signature), nil
}
