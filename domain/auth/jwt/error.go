package jwt

import (
	"somecome/pkg/errs"
)

var (
	JWTErrorGroup = errs.NewGroup("jwt")
)
