package jwt

import (
	"encoding/base64"
	"fmt"
)

func decodeJWTSegment(segment string) ([]byte, error) {
	decodedSegment, err := base64.RawURLEncoding.DecodeString(segment)
	if err != nil {
		return nil, err
	}

	return decodedSegment, nil
}

func decodeJWTSegments(segments []string) ([][]byte, error) {
	result := make([][]byte, 0, len(segments))

	for index, segmant := range segments {
		data, err := decodeJWTSegment(segmant)
		if err != nil {
			return nil, fmt.Errorf("decode segment %d: %w", index, err)
		}

		result = append(result, data)
	}

	return result, nil
}

func jwtPartWithoutSignatureFromRaw(rawHeader, rawPayload []byte) []byte {
	data := encodeJWTSegmentRaw(rawHeader)
	data = append(data, []byte(".")...)
	data = append(data, encodeJWTSegmentRaw(rawPayload)...)
	return data
}

func encodeJWTSegmentRaw(segment []byte) []byte {
	data := make([]byte, base64.RawURLEncoding.EncodedLen(len(segment)))

	base64.RawURLEncoding.Encode(data, segment)

	return data
}

func encodeJWTSegment(segment []byte) string {
	return base64.RawURLEncoding.EncodeToString(segment)
}
