package jwt

import (
	"crypto"
	"crypto/ed25519"
	"crypto/rand"
)

func signToken(privateKey ed25519.PrivateKey, rawData []byte) ([]byte, error) {
	// crypto.Hash(0) нужна для работы подписания ed25519, с другими вариантами хеширования он не работает!
	signature, err := privateKey.Sign(rand.Reader, rawData, crypto.Hash(0))
	if err != nil {
		return nil, err
	}

	return signature, nil
}

func checkTokenSign(rawData, signature []byte, publicKey ed25519.PublicKey) bool {
	return ed25519.Verify(publicKey, rawData, signature)
}

func NewKeyPair() (ed25519.PublicKey, ed25519.PrivateKey, error) {
	publicKey, privateKey, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		return nil, nil, err
	}

	return publicKey, privateKey, nil
}
