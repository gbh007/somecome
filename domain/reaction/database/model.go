package database

import (
	"database/sql"
	"time"
)

// Reaction - данные о реакции
type Reaction struct {
	// ИД реакции
	ID int64 `db:"id"`
	// Название реакции
	Name string `db:"name"`
	// Ид пользователя создавшего реакцию
	CreatorID int64 `db:"creator_id"`
	// Токен файла из хранилища
	FileToken string `db:"file_token"`
	// Время создания реакции
	Created time.Time `db:"created"`
	// Время последнего обновления реакции
	Updated sql.NullTime `db:"updated"`
}

// ContentReaction - реакции на контент
type ContentReaction struct {
	// Ключ контента
	ContentKey string `db:"content_key"`
	// Ид пользователя
	UserID int64 `db:"user_id"`
	// Ид пользователя
	ReactionID int64 `db:"reaction_id"`
	// Комментарий к реакции
	Comment sql.NullString `db:"comment"`
	// Время создания реакции
	Created time.Time `db:"created"`
	// Время последнего обновления реакции
	Updated sql.NullTime `db:"updated"`
}
