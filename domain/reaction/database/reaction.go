package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// CreateReaction - создает запись о новом реакции в БД
func (d *Database) CreateReaction(ctx context.Context, name string, userID int64, fileToken string) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO reaction.reactions(creator_id, name, file_token, created) VALUES ($1, $2, $3, $4) RETURNING id;`, userID, name, fileToken, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// SelectReactions - получает список реакций
func (d *Database) SelectReactions(ctx context.Context) ([]*Reaction, error) {
	list := make([]*Reaction, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM reaction.reactions ORDER BY id;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}

// SelectReaction - получает реакцию по ее ИД
func (d *Database) SelectReaction(ctx context.Context, id int64) (*Reaction, error) {
	data := new(Reaction)

	err := d.db.GetContext(ctx, data, `SELECT * FROM reaction.reactions WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return data, nil
}
