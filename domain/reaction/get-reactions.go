package reaction

import (
	"context"
	"somecome/domain/reaction/database"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/pkg/errs"
	"somecome/pkg/sqlconvert"
)

func reactionFromModel(raw *database.Reaction) *reactionDTO.Reaction {
	return &reactionDTO.Reaction{
		ID:        raw.ID,
		Name:      raw.Name,
		CreatorID: raw.CreatorID,
		FileToken: raw.FileToken,
		Created:   raw.Created,
		Updated:   sqlconvert.FromNullTime(raw.Updated),
	}
}

// GetReactions - получает список реакций
func (domain *Domain) GetReactions(ctx context.Context) ([]*reactionDTO.Reaction, error) {
	list := make([]*reactionDTO.Reaction, 0)

	reactions, err := domain.storage.SelectReactions(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	for _, reaction := range reactions {
		list = append(list, reactionFromModel(reaction))
	}

	return list, nil
}

// GetReaction - получает реакцию по ИД
func (domain *Domain) GetReaction(ctx context.Context, id int64) (*reactionDTO.Reaction, error) {
	reaction, err := domain.storage.SelectReaction(ctx, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return reactionFromModel(reaction), nil
}
