package apiHandlers

import "somecome/superobject"

type Object struct {
	superObject superobject.SuperObject
}

func Init(obj superobject.SuperObject) *Object {
	return &Object{
		superObject: obj,
	}
}
