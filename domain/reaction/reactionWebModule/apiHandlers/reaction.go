package apiHandlers

import (
	"errors"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	reactionDTO "somecome/domain/reaction/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"sort"
)

func (o *Object) switchContentReactionHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		request := new(ContentReactionInput)
		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = o.superObject.ReactionDomain().ContentReactionSwitch(ctx, reactionDTO.CRInput{
			ContentKey: request.ContentKey,
			ReactionID: request.ReactionID,
			UserID:     user.ID,
			Comment:    request.Comment,
		})
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}
		wsd.WriteNoContent(w)
	})
}

func (o *Object) createContentReactionHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		request := new(ContentReactionInput)
		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = o.superObject.ReactionDomain().CreateContentReaction(ctx, reactionDTO.CRInput{
			ContentKey: request.ContentKey,
			ReactionID: request.ReactionID,
			UserID:     user.ID,
			Comment:    request.Comment,
		})
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteNoContent(w)
	})
}

func (o *Object) listReactionHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		reactions, err := o.superObject.ReactionDomain().GetReactions(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		out := make([]ReactionOutput, 0, len(reactions))

		for _, reaction := range reactions {
			out = append(out, ReactionOutput{
				ID:          reaction.ID,
				Name:        reaction.Name,
				PreviewLink: link.GetReactionLink(reaction.ID),
			})
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, out)
	})
}

func (o *Object) listCompressedReactionHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		request := new(CRCompressedInfoInput)

		var userID *int64

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil && !errors.Is(err, authDTO.UserNotFoundErr) {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		if user != nil {
			userID = &user.ID
		}

		err = wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		reactions, err := o.superObject.ReactionDomain().ContentReactionCompressed(ctx, request.ContentKey, userID)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		out := make([]CRCompressedInfoOutput, 0, len(reactions))

		for _, reaction := range reactions {
			out = append(out, CRCompressedInfoOutput{
				ContentKey:  reaction.ContentKey,
				ReactionID:  reaction.ReactionID,
				Count:       reaction.Count,
				Used:        reaction.Used,
				PreviewLink: link.GetReactionLink(reaction.ReactionID),
			})
		}

		sort.Slice(out, func(i, j int) bool {
			return out[i].ReactionID < out[j].ReactionID
		})

		wsd.WriteJSON(ctx, w, http.StatusOK, out)
	})
}
