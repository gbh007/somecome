package apiHandlers

import (
	"net/http"
	"somecome/domain/webserver/core"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle("/api/reaction/switch", core.PostWithNoOptionWrapper(wsd, o.switchContentReactionHandler(wsd)))
	mux.Handle("/api/reaction/create", core.PostWithNoOptionWrapper(wsd, o.createContentReactionHandler(wsd)))
	mux.Handle("/api/reaction/list", core.GetWithNoOptionWrapper(wsd, o.listReactionHandler(wsd)))
	mux.Handle("/api/reaction/compressed", core.PostWithNoOptionWrapper(wsd, o.listCompressedReactionHandler(wsd)))
}
