package pageHandlers

import (
	"time"
)

// ReactionListTD - данные для рендеринга страницы со списком реакций
type ReactionListTD struct {
	Reactions []ReactionListUnit
}

// UserUnit - данные пользователя для отображения
type UserUnit struct {
	// ИД пользователя
	ID int64
	// Аватарка пользователя
	AvatarURL *string
	// Отображаемое имя
	DisplayName string
}

// ReactionListUnit - данные о реакции
type ReactionListUnit struct {
	// ИД реакции
	ID int64
	// Название реакции
	Name string
	// Файл с реакцией
	File FileInfoUnit
	// Данные о создателе реакции
	Creator UserUnit
	// Время создания реакции
	Created time.Time
}

// ReactionSimpleListUnit - данные о реакции упрощенные
type ReactionSimpleListUnit struct {
	// ИД реакции
	ID int64
	// Название реакции
	Name string
	// Ссылка на предпросмотр
	PreviewLink string
}

// ReactionCreateFMRTD - данные для рендеринга страницы создания реакции на контент
type ReactionCreateFMRTD struct {
	// Ключ контента
	ContentKey string
	// Ссылка на которую нужно перенаправить после реакции
	RedirectLink string
	// Реакции для выбора
	Reactions []ReactionSimpleListUnit
	// Ссылка для сохранения
	SaveLink string
}
