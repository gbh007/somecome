package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToCreateReaction, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.createReactionPageGet(wsd),
			http.MethodPost: o.createReactionPagePost(wsd),
		},
	))
	mux.Handle(PathToReactionList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.listReactionPageGet(wsd),
		},
	))
	mux.Handle(PathToCreateReactionOnContent, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet:  o.createFMRPageGet(wsd),
			http.MethodPost: o.createFMRPagePost(wsd),
		},
	))
}
