package reaction

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена реакций
	DomainError = errs.NewGroup("reaction domain")
)
