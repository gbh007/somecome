package reaction

import (
	"context"
	"somecome/config"
	"somecome/domain/reaction/database"
	"somecome/domain/reaction/reactionWebModule"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с реакциями
type Domain struct {
	// БД
	storage *database.Database
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *reactionWebModule.Module
}

// Name - название домена
func (_ *Domain) Name() string { return "reaction" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = reactionWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.storage = database.New(db)

	return nil
}
