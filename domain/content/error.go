package content

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена контента
	DomainError = errs.NewGroup("content domain")
)
