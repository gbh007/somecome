package contentDTO

import "errors"

var (
	// Ключ контента уже существует
	ContentKeyAlreadyExistsErr = errors.New("content key already exists")
	// Не уникальный ключ контента
	NotUniqueContentKeyErr = errors.New("not unique content key")
	// Сообщение не создано пользователем
	NotAuthorErr = errors.New("not author")
)
