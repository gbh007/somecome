package contentDTO

import (
	"time"
)

// Tag - данные тега
type Tag struct {
	// ИД тега
	ID int64
	// Текст тега
	Tag string
	// Цвет фона тега
	Color string
	// Цвет текста тега
	TextColor string
	// Ид пользователя создавшего тег
	CreatorID int64
	// Время создания тега
	Created time.Time
	// Время последнего обновления тега
	Updated *time.Time
}

// TagInput - данные для создания/изменения тега
type TagInput struct {
	// ИД тега
	ID int64
	// Текст тега
	Tag string
	// Цвет фона тега
	Color string
	// Цвет текста тега
	TextColor string
	// Ид пользователя создавшего/изменившего тег
	UserID int64
}
