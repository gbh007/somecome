package apiHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToTaggedContent, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodOptions: wsd.NoContentMock(),
			http.MethodGet:     o.taggedContentGet(wsd),
			http.MethodPost: wsd.PermissionJSONHandler(
				o.taggedContentPost(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelFullAccess,
			),
		},
	))
	mux.Handle(PathToTagListContent, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodOptions: wsd.NoContentMock(),
			http.MethodGet:     o.tagListGet(wsd),
		},
	))
}
