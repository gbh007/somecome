package apiHandlers

import (
	"net/http"
	"somecome/superobject"
)

// Tag - данные тега
type Tag struct {
	// ИД тега
	ID int64 `json:"id"`
	// Текст тега
	Tag string `json:"tag"`
	// Цвет фона тега
	Color string `json:"color"`
	// Цвет текста тега
	TextColor string `json:"text_color"`
}

func (o *Object) taggedContentGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		contentKey := r.URL.Query().Get("content-key")
		tags, err := o.superObject.ContentDomain().ContentTags(ctx, contentKey)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		out := make([]Tag, 0, len(tags))

		for _, tag := range tags {
			out = append(out, fromDTO(tag))
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, out)
	})
}

// TaggedInput - данные для тегирования
type TaggedInput struct {
	// ИД тега
	TagID int64 `json:"tag_id"`
	// Ключ контента
	ContentKey string `json:"content_key"`
}

func (o *Object) taggedContentPost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		request := new(TaggedInput)

		err := wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = o.superObject.ContentDomain().AddTagToContent(ctx, request.TagID, request.ContentKey, user.ID)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteNoContent(w)
	})
}
