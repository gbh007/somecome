package apiHandlers

import (
	"net/http"
	contentDTO "somecome/domain/content/dto"
	"somecome/superobject"
)

func (o *Object) tagListGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		tags, err := o.superObject.ContentDomain().GetTags(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		out := make([]Tag, 0, len(tags))

		for _, tag := range tags {
			out = append(out, fromDTO(tag))
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, out)
	})
}

func fromDTO(raw *contentDTO.Tag) Tag {
	return Tag{
		ID:        raw.ID,
		Tag:       raw.Tag,
		Color:     raw.Color,
		TextColor: raw.TextColor,
	}
}
