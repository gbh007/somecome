package pageHandlers

import (
	"net/http"
	contentDTO "somecome/domain/content/dto"
	"somecome/domain/webserver/link"
	"somecome/superobject"
	"strconv"
)

// TagEditorTD - данные для редактора тегов
type TagEditorTD struct {
	SimpleTagUnit
	// Ссылка на сохранение тега
	SaveLink string
}

func (o *Object) tagEditorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, TagEditorPageName)

		var (
			tagID int64
			err   error
		)

		rawID := r.URL.Query().Get("id")
		if rawID != "" && rawID != "new" {
			tagID, err = strconv.ParseInt(rawID, 10, 64)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

				return
			}
		}

		data := TagEditorTD{
			SaveLink: PathToTagEditor,
		}

		// Новый тег
		if tagID == 0 {
			tdata.SetData(data)
			wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)

			return
		}

		tagInfo, err := o.superObject.ContentDomain().GetTag(ctx, tagID)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		data.SimpleTagUnit = fromDTO(tagInfo)

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) tagEditorPagePost(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, link.EmptyPageTemplateName)

		userData, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		text := r.FormValue("tag-text")
		color := r.FormValue("tag-color")
		textColor := r.FormValue("tag-text-color")
		actionType := r.FormValue("at")

		tagID, err := strconv.ParseInt(r.FormValue("tag-id"), 10, 64)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

			return
		}

		tagInput := contentDTO.TagInput{
			ID:        tagID,
			Tag:       text,
			Color:     color,
			TextColor: textColor,
			UserID:    userData.ID,
		}

		// Новый тег
		if tagID < 1 {
			tagID, err = o.superObject.ContentDomain().CreateTag(ctx, tagInput)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		} else {
			err = o.superObject.ContentDomain().UpdateTag(ctx, tagInput)
			if err != nil {
				tdata.WithError(err)
				wsd.WriteTemplate(ctx, w, http.StatusBadRequest, tdata)

				return
			}
		}

		redirectTo := PathToTagList

		if actionType == "save" {
			redirectTo = GetTagEditorLink(tagID, false)
		}

		http.Redirect(w, r, redirectTo, http.StatusSeeOther)
	})
}
