package pageHandlers

import (
	contentDTO "somecome/domain/content/dto"
	"time"
)

// SimpleTagUnit - данные тега
type SimpleTagUnit struct {
	// ИД тега
	ID int64
	// Текст тега
	Tag string
	// Цвет фона тега
	Color string
	// Цвет текста тега
	TextColor string
	// Ид пользователя создавшего тег
	CreatorID int64
	// Время создания тега
	Created time.Time
	// Время последнего обновления тега
	Updated *time.Time
}

func fromDTO(raw *contentDTO.Tag) SimpleTagUnit {
	return SimpleTagUnit{
		ID:        raw.ID,
		Tag:       raw.Tag,
		Color:     raw.Color,
		TextColor: raw.TextColor,
		CreatorID: raw.CreatorID,
		Created:   raw.Created,
		Updated:   raw.Updated,
	}
}
