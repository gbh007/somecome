package pageHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToTagList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.tagListPageGet(wsd),
		},
	))
	mux.Handle(PathToTagEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(
				o.tagEditorPageGet(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelFullAccess,
			),
			http.MethodPost: wsd.PermissionTDHandler(
				o.tagEditorPagePost(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelFullAccess,
			),
		},
	))
	mux.Handle(PathToTagger, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(
				o.taggerPageGet(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelFullAccess,
			),
			http.MethodPost: wsd.PermissionTDHandler(
				o.taggerPagePost(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelFullAccess,
			),
		},
	))
	mux.Handle(PathToContentTags, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: o.contentTagsPageGet(wsd),
		},
	))
	mux.Handle(PathToContentSearch, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(
				o.contentSearchPageGet(wsd),
				authDTO.TestingAlphaAccess,
				authDTO.LevelReadOnly,
			),
		},
	))
}
