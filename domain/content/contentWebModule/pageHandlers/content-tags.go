package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

// ContentTagsTD - данные для списка тегов контента
type ContentTagsTD struct {
	// Теги
	Tags []SimpleTagUnit
	// Ссылка на привязку тега
	TaggerLink string
}

func (o *Object) contentTagsPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, ContentTagsPageName)

		contentKey := r.URL.Query().Get("content-key")
		redirect := r.URL.Query().Get("redirect")

		tags, err := o.superObject.ContentDomain().ContentTags(ctx, contentKey)
		if err != nil {
			tdata.WithError(err)
			wsd.WriteTemplate(ctx, w, http.StatusInternalServerError, tdata)

			return
		}

		data := ContentTagsTD{
			TaggerLink: GetTaggerLink(contentKey, redirect),
		}

		for _, tag := range tags {
			data.Tags = append(data.Tags, fromDTO(tag))
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
