package content

import (
	"context"
	"somecome/config"
	"sync"

	"somecome/domain/content/contentWebModule"
	"somecome/domain/content/database"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с контентом
type Domain struct {
	// БД
	storage *database.Database
	// Супер объект
	superObject superobject.SuperObject
	// Веб модуль
	module *contentWebModule.Module

	// Мьютекс для защиты мапы ключей контента
	keyMutex *sync.Mutex
	// Мапа ключей контента
	keyMap map[string]*innerContentKey
}

// Name - название домена
func (_ *Domain) Name() string { return "content" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = contentWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.keyMutex = &sync.Mutex{}
	domain.keyMap = make(map[string]*innerContentKey)

	domain.storage = database.New(db)

	return nil
}
