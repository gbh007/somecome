package database

import (
	"context"
	"somecome/pkg/errs"
	"time"
)

// SelectTag - получает тег по его тексту
func (d *Database) SelectTag(ctx context.Context, tagText string) (*Tag, error) {
	tag := new(Tag)

	err := d.db.GetContext(ctx, tag, `SELECT * FROM content.tags WHERE tag = $1 LIMIT 1;`, tagText)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return tag, nil
}

// SelectTagByID - получает тег по его тексту
func (d *Database) SelectTagByID(ctx context.Context, id int64) (*Tag, error) {
	tag := new(Tag)

	err := d.db.GetContext(ctx, tag, `SELECT * FROM content.tags WHERE id = $1 LIMIT 1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return tag, nil
}

// InsertTag - создает новый тег в БД
func (d *Database) InsertTag(ctx context.Context, tagText string, color, colorText string, userID int64) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO content.tags(tag, color, text_color, creator_id, created)
	 VALUES ($1, $2, $3, $4, $5) RETURNING id;`, tagText, color, colorText, userID, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdateTag - обновляет данные тега в БД
func (d *Database) UpdateTag(ctx context.Context, tagID int64, tagText string, color, colorText string) error {
	_, err := d.db.ExecContext(ctx, `UPDATE content.tags SET tag = $2, color = $3, text_color = $4, updated = $5 WHERE id = $1;`, tagID, tagText, color, colorText, time.Now())
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectTags - получает все теги
func (d *Database) SelectTags(ctx context.Context) ([]*Tag, error) {
	list := make([]*Tag, 0)

	err := d.db.SelectContext(ctx, &list, `SELECT * FROM content.tags ORDER BY id;`)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return list, nil
}
