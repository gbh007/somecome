package database

import (
	"database/sql"
	"time"
)

// Tag - данные тега
type Tag struct {
	// ИД тега
	ID int64 `db:"id"`
	// Текст тега
	Tag string `db:"tag"`
	// Цвет фона тега
	Color string `db:"color"`
	// Цвет текста тега
	TextColor string `db:"text_color"`
	// Ид пользователя создавшего тег
	CreatorID int64 `db:"creator_id"`
	// Время создания тега
	Created time.Time `db:"created"`
	// Время последнего обновления тега
	Updated sql.NullTime `db:"updated"`
}

// TaggedContent - тегированый контент
type TaggedContent struct {
	// ИД тега
	TagID int64 `db:"tag_id"`
	// Ключ контента
	ContentKey string `db:"content_key"`
	// Ид пользователя создавшего привязку
	CreatorID int64 `db:"creator_id"`
	// Время создания связки
	Created time.Time `db:"created"`
}
