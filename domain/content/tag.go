package content

import (
	"context"
	contentDTO "somecome/domain/content/dto"
	"somecome/pkg/errs"
)

// GetTags - все теги в системе
func (domain *Domain) GetTags(ctx context.Context) ([]*contentDTO.Tag, error) {
	rawTags, err := domain.storage.SelectTags(ctx)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	tags := make([]*contentDTO.Tag, 0, len(rawTags))

	for _, tag := range rawTags {
		tags = append(tags, fromModel(tag))
	}

	return tags, nil
}

// CreateTag - создает новый тег
func (domain *Domain) CreateTag(ctx context.Context, tagInfo contentDTO.TagInput) (int64, error) {
	// TODO: валидация формата цвета
	id, err := domain.storage.InsertTag(ctx, tagInfo.Tag, tagInfo.Color, tagInfo.TextColor, tagInfo.UserID)
	if err != nil {
		return 0, errs.WrapError(ctx, DomainError, err)
	}

	return id, nil
}

// UpdateTag - обновляет тег
func (domain *Domain) UpdateTag(ctx context.Context, tagInfo contentDTO.TagInput) error {
	// TODO: валидация формата цвета
	oldTag, err := domain.storage.SelectTagByID(ctx, tagInfo.ID)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	// Не является создателем
	if oldTag.CreatorID != tagInfo.UserID {
		return errs.WrapError(ctx, DomainError, contentDTO.NotAuthorErr)
	}

	err = domain.storage.UpdateTag(ctx, tagInfo.ID, tagInfo.Tag, tagInfo.Color, tagInfo.TextColor)
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// GetTag - возвращает тег по его ид
func (domain *Domain) GetTag(ctx context.Context, tagID int64) (*contentDTO.Tag, error) {
	rawTag, err := domain.storage.SelectTagByID(ctx, tagID)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return fromModel(rawTag), nil
}
