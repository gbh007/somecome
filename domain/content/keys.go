package content

import (
	"context"
	contentDTO "somecome/domain/content/dto"
	"somecome/pkg/errs"
	"strings"
)

// innerContentKey - ключ контента
type innerContentKey struct {
	// Префикс ключа
	Prefix string
	// Генерация ссылки по значению ключа
	GetLink func(ctx context.Context, v string) string
	// Генерация названия по значению ключа
	GetName func(ctx context.Context, v string) string
	// Генерация описания по значению ключа
	GetDescription func(ctx context.Context, v string) string
}

// RegisterKey - регистрирует ключ контента
func (domain *Domain) RegisterKey(ctx context.Context, key contentDTO.ContentKey) error {
	domain.keyMutex.Lock()
	defer domain.keyMutex.Unlock()

	// Проверка на существования ключа
	_, exists := domain.keyMap[key.Prefix]
	if exists {
		return errs.WrapError(ctx, DomainError, contentDTO.ContentKeyAlreadyExistsErr)
	}

	// Проверка на включение ключей друг в друга
	for existsPrefix := range domain.keyMap {
		if strings.HasPrefix(existsPrefix, key.Prefix) ||
			strings.HasPrefix(key.Prefix, existsPrefix) {
			return errs.WrapError(ctx, DomainError, contentDTO.NotUniqueContentKeyErr)
		}
	}

	domain.keyMap[key.Prefix] = &innerContentKey{
		Prefix:         key.Prefix,
		GetLink:        key.GetLink,
		GetName:        key.GetName,
		GetDescription: key.GetDescription,
	}

	return nil
}

// GetKeyLink - возвращает ссылку на контент по его ключу
func (domain *Domain) GetKeyLink(ctx context.Context, contentKey string) *string {
	domain.keyMutex.Lock()
	defer domain.keyMutex.Unlock()

	for prefix, ckey := range domain.keyMap {
		if strings.HasPrefix(contentKey, prefix) && ckey.GetLink != nil {
			link := ckey.GetLink(ctx, strings.TrimPrefix(contentKey, prefix))

			return &link
		}
	}

	return nil
}

// GetKeyName - возвращает название контента по его ключу
func (domain *Domain) GetKeyName(ctx context.Context, contentKey string) *string {
	domain.keyMutex.Lock()
	defer domain.keyMutex.Unlock()

	for prefix, ckey := range domain.keyMap {
		if strings.HasPrefix(contentKey, prefix) && ckey.GetName != nil {
			name := ckey.GetName(ctx, strings.TrimPrefix(contentKey, prefix))

			return &name
		}
	}

	return nil
}

// GetKeyDescription - возвращает описание контента по его ключу
func (domain *Domain) GetKeyDescription(ctx context.Context, contentKey string) *string {
	domain.keyMutex.Lock()
	defer domain.keyMutex.Unlock()

	for prefix, ckey := range domain.keyMap {
		if strings.HasPrefix(contentKey, prefix) && ckey.GetDescription != nil {
			name := ckey.GetDescription(ctx, strings.TrimPrefix(contentKey, prefix))

			return &name
		}
	}

	return nil
}
