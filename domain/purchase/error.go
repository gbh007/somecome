package purchase

import (
	"somecome/pkg/errs"
)

var (
	// Ошибка домена покупок
	DomainError = errs.NewGroup("purchase domain")
)
