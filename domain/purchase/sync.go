package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
)

// syncItems - синхронизирует товары в покупке с данными в БД
func (domain *Domain) syncItems(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) error {
	oldItemsRaw, err := domain.storage.ListPurchaseItem(ctx, purchase.ID)
	if err != nil {
		return err
	}

	oldItems := make(map[int64]struct{})

	for _, item := range oldItemsRaw {
		oldItems[item.ID] = struct{}{}
	}

	for _, item := range purchase.Items {
		item.PurchaseID = purchase.ID

		// Данные есть, требуется обновление
		if _, exist := oldItems[item.ID]; exist {
			// Удаляем из списка старых данных
			delete(oldItems, item.ID)

			err = domain.storage.UpdatePurchaseItem(ctx, itemToRepo(item, creatorID))
			if err != nil {
				return err
			}
		} else {
			// Данных нет, требуется создание
			itemID, err := domain.storage.CreatePurchaseItem(ctx, itemToRepo(item, creatorID))
			if err != nil {
				return err
			}

			item.ID = itemID
		}
	}

	// Товары не используются в покупке, удаляем
	for itemID := range oldItems {
		err = domain.storage.DeletePurchaseItem(ctx, itemID)
		if err != nil {
			return err
		}
	}

	return nil
}

// syncPayments - синхронизирует способы оплаты с данными в БД
func (domain *Domain) syncPayments(ctx context.Context, purchase *purchaseDTO.Purchase, creatorID int64) error {
	oldPaymentsRaw, err := domain.storage.ListPurchasePayment(ctx, purchase.ID)
	if err != nil {
		return err
	}

	oldPayments := make(map[int64]struct{})

	for _, payment := range oldPaymentsRaw {
		oldPayments[payment.ID] = struct{}{}
	}

	for _, payment := range purchase.Payments {
		payment.PurchaseID = purchase.ID

		// Данные есть, требуется обновление
		if _, exist := oldPayments[payment.ID]; exist {
			// Удаляем из списка старых данных
			delete(oldPayments, payment.ID)

			err = domain.storage.UpdatePurchasePayment(ctx, paymentToRepo(payment, creatorID))
			if err != nil {
				return err
			}
		} else {
			// Данных нет, требуется создание
			itemID, err := domain.storage.CreatePurchasePayment(ctx, paymentToRepo(payment, creatorID))
			if err != nil {
				return err
			}

			payment.ID = itemID
		}
	}

	// Оплата не используются в покупке, удаляем
	for paymentID := range oldPayments {
		err = domain.storage.DeletePurchasePayment(ctx, paymentID)
		if err != nil {
			return err
		}
	}

	return nil
}
