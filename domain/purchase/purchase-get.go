package purchase

import (
	"context"
	"fmt"
	purchaseDTO "somecome/domain/purchase/dto"
	"somecome/pkg/errs"
	"somecome/pkg/paginator"
)

// GetPurchase - возвращает данные покупки
func (domain *Domain) GetPurchase(ctx context.Context, id int64, full bool) (*purchaseDTO.Purchase, error) {
	rawPurchase, err := domain.storage.GetPurchase(ctx, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	purchase := purchaseFromRepo(rawPurchase)

	if rawPurchase.OrganizationID.Valid {
		org, err := domain.storage.GetOrganizationByID(ctx, rawPurchase.OrganizationID.Int64)
		if err != nil {
			return nil, errs.WrapError(ctx, DomainError, err)
		}

		purchase.OrganizationName = org.Name
	}

	if !full {
		return purchase, nil
	}

	toWrap := []*purchaseDTO.Purchase{purchase}

	toWrap, err = domain.withItems(ctx, toWrap)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	toWrap, err = domain.withPayments(ctx, toWrap)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	if len(toWrap) != 1 {
		return nil, errs.WrapError(ctx, DomainError, fmt.Errorf("wrapping failed, len=%d", len(toWrap)))
	}

	return toWrap[0], nil
}

// ListPurchases - возвращает список покупок
func (domain *Domain) ListPurchases(ctx context.Context, creatorID int64, page int64, full bool) ([]*purchaseDTO.Purchase, error) {
	limit, offset := paginator.PageToLimit(page, paginator.DefaultOnPageCount)

	rawPurchases, err := domain.storage.ListPurchase(ctx, creatorID, limit, offset)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	purchases := make([]*purchaseDTO.Purchase, 0, len(rawPurchases))

	for _, rawPurchase := range rawPurchases {
		purchase := purchaseFromRepo(rawPurchase)

		if rawPurchase.OrganizationID.Valid {
			org, err := domain.storage.GetOrganizationByID(ctx, rawPurchase.OrganizationID.Int64)
			if err != nil {
				return nil, errs.WrapError(ctx, DomainError, err)
			}

			purchase.OrganizationName = org.Name
		}

		purchases = append(purchases, purchase)
	}

	if !full {
		return purchases, nil
	}

	purchases, err = domain.withItems(ctx, purchases)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	purchases, err = domain.withPayments(ctx, purchases)
	if err != nil {
		return nil, errs.WrapError(ctx, DomainError, err)
	}

	return purchases, nil
}

// SelectPurchaseCount - получает количество покупок
func (domain *Domain) PurchaseCount(ctx context.Context, creatorID int64) (int64, int64, error) {
	count, err := domain.storage.SelectPurchaseCount(ctx, creatorID)
	if err != nil {
		return 0, 0, errs.WrapError(ctx, DomainError, err)
	}

	pageCount := paginator.TotalToPages(count, paginator.DefaultOnPageCount)

	return count, pageCount, nil
}
