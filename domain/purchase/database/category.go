package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Category - категория товара.
type Category struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// Название категории
	Name string `db:"name"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreateCategory - создает в базе новую запись о категории
func (d *Database) CreateCategory(ctx context.Context, creatorID int64, name string) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO purchase.categories (creator_id, name, created) VALUES ($1, $2, $3) RETURNING id;`, creatorID, name, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// ListCategories - возвращает список всех категорий в базе
func (d *Database) ListCategories(ctx context.Context, creatorID int64) ([]*Category, error) {
	result := make([]*Category, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.categories WHERE creator_id = $1 ORDER BY name;`, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}
