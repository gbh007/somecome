package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// PurchaseItem - товар в покупке.
type PurchaseItem struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// ИД покупки
	PurchaseID int64 `db:"purchase_id"`
	// ИД категории товара
	CategoryID sql.NullInt64 `db:"category_id"`
	// Название товара
	Name string `db:"name"`
	// Цена товара
	Price float64 `db:"price"`
	// Количество товара
	Amount float64 `db:"amount"`
	// Суммарная скидка на товар
	Sale float64 `db:"sale"`
	// Итоговая цена товара
	Total float64 `db:"total"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreatePurchaseItem - создает в базе новую запись о товаре
func (d *Database) CreatePurchaseItem(ctx context.Context, item *PurchaseItem) (int64, error) {
	var id int64

	item.Created = time.Now()

	query, args, err := d.db.BindNamed(`INSERT INTO purchase.purchase_items (
		creator_id,
		purchase_id,
		category_id,
		name,
		price,
		amount,
		sale,
		total,
		created
	) VALUES (
		:creator_id,
		:purchase_id,
		:category_id,
		:name,
		:price,
		:amount,
		:sale,
		:total,
		:created
	) RETURNING id;`, item)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	err = d.db.GetContext(ctx, &id, query, args...)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdatePurchaseItem - обновляет в базе запись о товаре
func (d *Database) UpdatePurchaseItem(ctx context.Context, item *PurchaseItem) error {
	item.Updated = sql.NullTime{Time: time.Now(), Valid: true}

	_, err := d.db.NamedExecContext(ctx, `UPDATE purchase.purchase_items SET
		purchase_id = :purchase_id,
		category_id = :category_id,
		name = :name,
		price = :price,
		amount = :amount,
		sale = :sale,
		total = :total,
		updated = :updated
	WHERE id = :id;`, item)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// ListPurchaseItem - возвращает список всех товаров в покупке
func (d *Database) ListPurchaseItem(ctx context.Context, purchaseID int64) ([]*PurchaseItem, error) {
	result := make([]*PurchaseItem, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.purchase_items WHERE purchase_id = $1 ORDER BY id;`, purchaseID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}

// DeletePurchaseItem - удаляет товар
func (d *Database) DeletePurchaseItem(ctx context.Context, id int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM purchase.purchase_items WHERE id = $1;`, id)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
