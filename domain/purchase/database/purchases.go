package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Purchase - покупка.
type Purchase struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// ИД организации в которой была совершена покупка
	OrganizationID sql.NullInt64 `db:"organization_id"`
	// Дата совершения платежа
	PaymentAt sql.NullTime `db:"payment_at"`
	// Итоговая сумма покупки
	Total sql.NullFloat64 `db:"total"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreatePurchase - создает в базе новую запись о покупке
func (d *Database) CreatePurchase(ctx context.Context, purchase *Purchase) (int64, error) {
	var id int64

	purchase.Created = time.Now()

	query, args, err := d.db.BindNamed(`INSERT INTO purchase.purchases (
		creator_id,
		organization_id,
		payment_at,
		total,
		created
	) VALUES (
		:creator_id,
		:organization_id,
		:payment_at,
		:total,
		:created
	) RETURNING id;`, purchase)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	err = d.db.GetContext(ctx, &id, query, args...)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdatePurchase - обновляет в базе запись о покупке
func (d *Database) UpdatePurchase(ctx context.Context, purchase *Purchase) error {
	purchase.Updated = sql.NullTime{Time: time.Now(), Valid: true}

	_, err := d.db.NamedExecContext(ctx, `UPDATE purchase.purchases SET
		organization_id = :organization_id,
		payment_at = :payment_at,
		total = :total,
		updated = :updated
	WHERE id = :id;`, purchase)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// ListPurchase - возвращает список покупок
func (d *Database) ListPurchase(ctx context.Context, creatorID int64, limit, offset int64) ([]*Purchase, error) {
	result := make([]*Purchase, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.purchases WHERE creator_id = $3 ORDER BY payment_at DESC, id DESC LIMIT $1 OFFSET $2;`, limit, offset, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}

// GetPurchase - возвращает покупку по ид
func (d *Database) GetPurchase(ctx context.Context, id int64) (*Purchase, error) {
	result := new(Purchase)

	err := d.db.GetContext(ctx, result, `SELECT * FROM purchase.purchases WHERE id = $1;`, id)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}

// DeletePurchase - удаляет покупку
func (d *Database) DeletePurchase(ctx context.Context, id int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM purchase.purchases WHERE id = $1;`, id)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// SelectPurchaseCount - получает количество покупок
func (d *Database) SelectPurchaseCount(ctx context.Context, creatorID int64) (int64, error) {
	var count int64

	err := d.db.GetContext(ctx, &count, `SELECT count(*) FROM purchase.purchases WHERE creator_id = $1;`, creatorID)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return count, nil
}
