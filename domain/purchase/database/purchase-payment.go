package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// PurchasePayment - оплата покупки.
type PurchasePayment struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// ИД покупки
	PurchaseID int64 `db:"purchase_id"`
	// ИД способа оплаты
	CardID sql.NullInt64 `db:"card_id"`
	// Оплаченная сумма
	Total float64 `db:"total"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreatePurchasePayment - создает в базе новую запись об оплате покупки
func (d *Database) CreatePurchasePayment(ctx context.Context, payment *PurchasePayment) (int64, error) {
	var id int64

	payment.Created = time.Now()

	query, args, err := d.db.BindNamed(`INSERT INTO purchase.purchase_payments (
		creator_id,
		purchase_id,
		card_id,
		total,
		created
	) VALUES (
		:creator_id,
		:purchase_id,
		:card_id,
		:total,
		:created
	) RETURNING id;`, payment)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	err = d.db.GetContext(ctx, &id, query, args...)
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// UpdatePurchasePayment - обновляет в базе запись об оплате
func (d *Database) UpdatePurchasePayment(ctx context.Context, payment *PurchasePayment) error {
	payment.Updated = sql.NullTime{Time: time.Now(), Valid: true}

	_, err := d.db.NamedExecContext(ctx, `UPDATE purchase.purchase_payments SET
		purchase_id = :purchase_id,
		card_id = :card_id,
		total = :total,
		updated = :updated
	WHERE id = :id;`, payment)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}

// ListPurchasePayment - возвращает список оплаты покупки
func (d *Database) ListPurchasePayment(ctx context.Context, purchaseID int64) ([]*PurchasePayment, error) {
	result := make([]*PurchasePayment, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.purchase_payments WHERE purchase_id = $1 ORDER BY id;`, purchaseID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}

// DeletePurchasePayment - удаляет оплату
func (d *Database) DeletePurchasePayment(ctx context.Context, id int64) error {
	_, err := d.db.ExecContext(ctx, `DELETE FROM purchase.purchase_payments WHERE id = $1;`, id)
	if err != nil {
		return errs.WrapError(ctx, DatabaseErr, err)
	}

	return nil
}
