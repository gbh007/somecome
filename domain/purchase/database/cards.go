package database

import (
	"context"
	"database/sql"
	"somecome/pkg/errs"
	"time"
)

// Card - способ оплаты.
type Card struct {
	// ИД в базе
	ID int64 `db:"id"`
	// ИД пользователя создавшего запись
	CreatorID int64 `db:"creator_id"`
	// Название способа
	Name string `db:"name"`
	// Время создания
	Created time.Time `db:"created"`
	// Время последнего обновления
	Updated sql.NullTime `db:"updated"`
}

// CreateCard - создает в базе новую запись о способе оплаты
func (d *Database) CreateCard(ctx context.Context, creatorID int64, name string) (int64, error) {
	var id int64

	err := d.db.GetContext(ctx, &id, `INSERT INTO purchase.cards (creator_id, name, created) VALUES ($1, $2, $3) RETURNING id;`, creatorID, name, time.Now())
	if err != nil {
		return 0, errs.WrapError(ctx, DatabaseErr, err)
	}

	return id, nil
}

// ListCards - возвращает список всех способов оплаты в базе
func (d *Database) ListCards(ctx context.Context, creatorID int64) ([]*Card, error) {
	result := make([]*Card, 0)

	err := d.db.SelectContext(ctx, &result, `SELECT * FROM purchase.cards WHERE creator_id = $1 ORDER BY name;`, creatorID)
	if err != nil {
		return nil, errs.WrapError(ctx, DatabaseErr, err)
	}

	return result, nil
}
