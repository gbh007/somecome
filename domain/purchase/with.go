package purchase

import (
	"context"
	purchaseDTO "somecome/domain/purchase/dto"
)

// withItems - вносит в данные покупки данные о товарах
func (domain *Domain) withItems(ctx context.Context, src []*purchaseDTO.Purchase) ([]*purchaseDTO.Purchase, error) {
	out := make([]*purchaseDTO.Purchase, 0, len(src))

	for _, purchase := range src {
		rawItems, err := domain.storage.ListPurchaseItem(ctx, purchase.ID)
		if err != nil {
			return nil, err
		}

		for _, rawItem := range rawItems {
			purchase.Items = append(purchase.Items, itemFromRepo(rawItem))
		}

		out = append(out, purchase)
	}

	return out, nil
}

// withPayments - вносит в данные покупки данные об оплате
func (domain *Domain) withPayments(ctx context.Context, src []*purchaseDTO.Purchase) ([]*purchaseDTO.Purchase, error) {
	out := make([]*purchaseDTO.Purchase, 0, len(src))

	for _, purchase := range src {
		rawPayments, err := domain.storage.ListPurchasePayment(ctx, purchase.ID)
		if err != nil {
			return nil, err
		}

		for _, rawPayment := range rawPayments {
			purchase.Payments = append(purchase.Payments, paymentFromRepo(rawPayment))
		}

		out = append(out, purchase)
	}

	return out, nil
}
