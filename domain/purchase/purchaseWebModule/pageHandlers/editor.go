package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) purchaseEditorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, PurchaseEditorPageName)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) cardEditorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, PurchaseCardEditorPageName)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) categoryEditorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, PurchaseCategoryEditorPageName)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) organizationEditorPageGet(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, PurchaseOrganizationEditorPageName)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
