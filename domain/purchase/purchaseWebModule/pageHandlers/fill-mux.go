package pageHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/purchase/purchaseShared"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle(PathToPurchaseList, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.purchaseListPageGet(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		},
	))
	mux.Handle(PathToPurchaseEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.purchaseEditorPageGet(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		},
	))
	mux.Handle(PathToPurchaseCardEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.cardEditorPageGet(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		},
	))
	mux.Handle(PathToPurchaseCategoryEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.categoryEditorPageGet(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		},
	))
	mux.Handle(PathToPurchaseOrganizationEditor, wsd.MethodsSplitter(
		map[string]http.Handler{
			http.MethodGet: wsd.PermissionTDHandler(o.organizationEditorPageGet(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		},
	))
}
