package purchaseWebModule

import (
	"context"
	"embed"
	"html/template"
	"os"
	"somecome/pkg/ctxench"
	"somecome/pkg/errs"
)

//go:embed template/*
var templateDir embed.FS

const debugTemplateDirPath = "domain/purchase/purchaseWebModule/template"

// FillTemplates - наполняет шаблоны для рендеринга страниц
func (_ *Module) FillTemplates(ctx context.Context, t *template.Template) error {
	var (
		err error
	)

	if ctxench.GetDeveloperMode(ctx) {
		_, err = t.ParseFS(os.DirFS(debugTemplateDirPath), "*.gohtml")
	} else {
		_, err = t.ParseFS(templateDir, "template/*.gohtml")
	}

	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}
