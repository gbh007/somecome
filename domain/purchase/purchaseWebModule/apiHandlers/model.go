package apiHandlers

import "time"

// CardInput - данные для создания способа оплаты (карты)
type CardInput struct {
	Name string `json:"name"`
}

// CardOutput - данные для рендеринга карты
type CardOutput struct {
	// ИД карты
	ID int64 `json:"id"`
	// Название карты
	Name string `json:"name"`
}

// CategoryInput - данные для создания категории товара
type CategoryInput struct {
	Name string `json:"name"`
}

// CategoryOutput - данные для рендеринга категории товара
type CategoryOutput struct {
	// ИД категории
	ID int64 `json:"id"`
	// Название категории
	Name string `json:"name"`
}

// OrganizationInput - данные для создания организации
type OrganizationInput struct {
	Name string `json:"name"`
}

// OrganizationOutput - данные для рендеринга организации
type OrganizationOutput struct {
	// ИД организации
	ID int64 `json:"id"`
	// Название организации
	Name string `json:"name"`
}

// PurchaseData - покупка.
type PurchaseData struct {
	// ИД в базе
	ID int64 `json:"id"`
	// ИД организации в которой была совершена покупка
	OrganizationID *int64 `json:"organization_id,omitempty"`
	// Дата совершения платежа
	PaymentAt *time.Time `json:"payment_at,omitempty"`
	// Итоговая сумма покупки
	Total *float64 `json:"total,omitempty"`
	// Время создания
	Created time.Time `json:"created"`
	// Время последнего обновления
	Updated *time.Time `json:"updated,omitempty"`

	// Оплата
	Payments []*PurchasePaymentData `json:"payments,omitempty"`
	// Товары
	Items []*PurchaseItemData `json:"items,omitempty"`
}

// PurchasePaymentData - оплата покупки.
type PurchasePaymentData struct {
	// ИД в базе
	ID int64 `json:"id"`
	// ИД покупки
	PurchaseID int64 `json:"purchase_id"`
	// ИД способа оплаты
	CardID *int64 `json:"card_id,omitempty"`
	// Оплаченная сумма
	Total float64 `json:"total"`
	// Время создания
	Created time.Time `json:"created"`
	// Время последнего обновления
	Updated *time.Time `json:"updated,omitempty"`
}

// PurchaseItemData - товар в покупке.
type PurchaseItemData struct {
	// ИД в базе
	ID int64 `json:"id"`
	// ИД покупки
	PurchaseID int64 `json:"purchase_id"`
	// ИД категории товара
	CategoryID *int64 `json:"category_id,omitempty"`
	// Название товара
	Name string `json:"name"`
	// Цена товара
	Price float64 `json:"price"`
	// Количество товара
	Amount float64 `json:"amount"`
	// Суммарная скидка на товар
	Sale float64 `json:"sale"`
	// Итоговая цена товара
	Total float64 `json:"total"`
	// Время создания
	Created time.Time `json:"created"`
	// Время последнего обновления
	Updated *time.Time `json:"updated,omitempty"`
}
