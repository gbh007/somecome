package apiHandlers

import (
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/purchase/purchaseShared"
	"somecome/superobject"
)

func (o *Object) FillMux(wsd superobject.WebServerDomain, mux *http.ServeMux) {
	mux.Handle("/api/purchase", wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodGet:     wsd.PermissionJSONHandler(o.purchaseGetHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly),
		http.MethodPost:    wsd.PermissionJSONHandler(o.purchasePostHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadWrite),
	}))
	mux.Handle("/api/purchase/card", wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodGet:     wsd.PermissionJSONHandler(o.cardGetHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly), // TODO: возвращают массивы, а по сигнатуре должны один объект, кривое API
		http.MethodPost:    wsd.PermissionJSONHandler(o.cardPostHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadWrite),
	}))
	mux.Handle("/api/purchase/category", wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodGet:     wsd.PermissionJSONHandler(o.categoryGetHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly), // TODO: возвращают массивы, а по сигнатуре должны один объект, кривое API
		http.MethodPost:    wsd.PermissionJSONHandler(o.categoryPostHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadWrite),
	}))
	mux.Handle("/api/purchase/organization", wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodGet:     wsd.PermissionJSONHandler(o.organizationGetHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadOnly), // TODO: возвращают массивы, а по сигнатуре должны один объект, кривое API
		http.MethodPost:    wsd.PermissionJSONHandler(o.organizationPostHandler(wsd), purchaseShared.PurchaseAccessPC, authDTO.LevelReadWrite),
	}))
}
