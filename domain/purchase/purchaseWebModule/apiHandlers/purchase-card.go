package apiHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) cardGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		list, err := o.superObject.PurchaseDomain().ListCard(ctx, user.ID)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		output := make([]CardOutput, 0, len(list))
		for _, item := range list {
			output = append(output, CardOutput{
				ID:   item.ID,
				Name: item.Name,
			})
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, output)
	})
}

func (o *Object) cardPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		request := new(CardInput)

		err = wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		_, err = o.superObject.PurchaseDomain().NewCard(ctx, user.ID, request.Name)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteNoContent(w)
	})
}
