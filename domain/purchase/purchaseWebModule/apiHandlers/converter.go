package apiHandlers

import purchaseDTO "somecome/domain/purchase/dto"

func itemToRender(src *purchaseDTO.Item) *PurchaseItemData {
	return &PurchaseItemData{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CategoryID: src.CategoryID,
		Name:       src.Name,
		Price:      src.Price,
		Amount:     src.Amount,
		Sale:       src.Sale,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    src.Updated,
	}
}

func itemToSchema(src *PurchaseItemData) *purchaseDTO.Item {
	return &purchaseDTO.Item{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CategoryID: src.CategoryID,
		Name:       src.Name,
		Price:      src.Price,
		Amount:     src.Amount,
		Sale:       src.Sale,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    src.Updated,
	}
}

func paymentToRender(src *purchaseDTO.Payment) *PurchasePaymentData {
	return &PurchasePaymentData{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CardID:     src.CardID,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    src.Updated,
	}
}

func paymentToSchema(src *PurchasePaymentData) *purchaseDTO.Payment {
	return &purchaseDTO.Payment{
		ID:         src.ID,
		PurchaseID: src.PurchaseID,
		CardID:     src.CardID,
		Total:      src.Total,
		Created:    src.Created,
		Updated:    src.Updated,
	}
}

func purchaseToRender(src *purchaseDTO.Purchase) *PurchaseData {
	p := &PurchaseData{
		ID:             src.ID,
		OrganizationID: src.OrganizationID,
		PaymentAt:      src.PaymentAt,
		Total:          src.Total,
		Created:        src.Created,
		Updated:        src.Updated,
	}

	for _, item := range src.Items {
		p.Items = append(p.Items, itemToRender(item))
	}

	for _, payment := range src.Payments {
		p.Payments = append(p.Payments, paymentToRender(payment))
	}

	return p
}

func purchaseToSchema(src *PurchaseData) *purchaseDTO.Purchase {
	p := &purchaseDTO.Purchase{
		ID:             src.ID,
		OrganizationID: src.OrganizationID,
		PaymentAt:      src.PaymentAt,
		Total:          src.Total,
		Created:        src.Created,
		Updated:        src.Updated,
	}

	for _, item := range src.Items {
		p.Items = append(p.Items, itemToSchema(item))
	}

	for _, payment := range src.Payments {
		p.Payments = append(p.Payments, paymentToSchema(payment))
	}

	return p
}
