package apiHandlers

import (
	"net/http"
	"somecome/superobject"
	"strconv"
)

func (o *Object) purchaseGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		idRaw := r.URL.Query().Get("id")

		id, err := strconv.ParseInt(idRaw, 10, 64)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		purchaseInfo, err := o.superObject.PurchaseDomain().GetPurchase(ctx, id, true)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, purchaseToRender(purchaseInfo))
	})
}

func (o *Object) purchasePostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		request := new(PurchaseData)

		user, err := o.superObject.AuthDomain().GetUserFromContext(ctx)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		err = wsd.ParseJSON(r, &request)
		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		var id int64

		if request.ID == 0 {
			id, err = o.superObject.PurchaseDomain().NewPurchase(ctx, purchaseToSchema(request), user.ID)
		} else {
			id = request.ID
			err = o.superObject.PurchaseDomain().UpdatePurchase(ctx, purchaseToSchema(request), user.ID)
		}

		if err != nil {
			wsd.WriteJSON(ctx, w, http.StatusBadRequest, err)

			return
		}

		wsd.WriteJSON(ctx, w, http.StatusOK, id)
	})
}
