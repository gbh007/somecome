package purchase

import (
	"context"
	"somecome/config"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/purchase/database"
	"somecome/domain/purchase/purchaseShared"
	"somecome/domain/purchase/purchaseWebModule"
	"somecome/pkg/errs"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

// Domain - домен для работы с покупкам
type Domain struct {
	// БД
	storage *database.Database
	// Веб модуль
	module *purchaseWebModule.Module
	// Супер объект
	superObject superobject.SuperObject
}

// Name - название домена
func (_ *Domain) Name() string { return "purchase" }

// InnerInit - инициализирует домен
func (domain *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	domain.superObject = obj
	domain.module = purchaseWebModule.InitModule(obj, domain.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.superObject.WebServerDomain().RegisterModule(ctx, domain.module)

	err := domain.superObject.AuthDomain().RegisterPermissions(ctx, authDTO.RegisterDomainInput{
		Code:        domain.Name(),
		Name:        "Покупки",
		Description: "Домен прав для покупок",
		Permissions: []authDTO.DomainPermission{
			{
				Code:        purchaseShared.PurchaseAccessPC,
				Name:        "Доступ до покупок",
				Description: "Базовый доступ пользователя до покупок",
			},
		},
	})
	if err != nil {
		return errs.WrapError(ctx, DomainError, err)
	}

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.storage = database.New(db)

	return nil
}
