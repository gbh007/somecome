package purchaseDTO

import (
	"time"
)

// Card - способ оплаты.
type Card struct {
	// ИД в базе
	ID int64
	// Название способа оплаты
	Name string
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time
}

// Category - категория товара.
type Category struct {
	// ИД в базе
	ID int64
	// Название категории
	Name string
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time
}

// Organization - организация.
type Organization struct {
	// ИД в базе
	ID int64
	// Название организации
	Name string
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time
}

// Purchase - покупка.
type Purchase struct {
	// ИД в базе
	ID int64
	// ИД организации в которой была совершена покупка
	OrganizationID *int64
	// Название организации в которой была совершена покупка
	OrganizationName string
	// Дата совершения платежа
	PaymentAt *time.Time
	// Итоговая сумма покупки
	Total *float64
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time

	// Оплата
	Payments []*Payment
	// Товары
	Items []*Item
}

// Payment - оплата покупки.
type Payment struct {
	// ИД в базе
	ID int64
	// ИД покупки
	PurchaseID int64
	// ИД способа оплаты
	CardID *int64
	// Оплаченная сумма
	Total float64
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time
}

// Item - товар в покупке.
type Item struct {
	// ИД в базе
	ID int64
	// ИД покупки
	PurchaseID int64
	// ИД категории товара
	CategoryID *int64
	// Название товара
	Name string
	// Цена товара
	Price float64
	// Количество товара
	Amount float64
	// Суммарная скидка на товар
	Sale float64
	// Итоговая цена товара
	Total float64
	// Время создания
	Created time.Time
	// Время последнего обновления
	Updated *time.Time
}
