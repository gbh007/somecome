package webserver

import (
	"fmt"
	"net/http"
	authDTO "somecome/domain/auth/dto"
	"somecome/domain/webserver/core"
	"somecome/pkg/ctxench"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"time"
)

// sessionHandler - обработчик для внесения данных пользователя в контекст
func (domain *Domain) sessionHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			user *authDTO.User
			err  error
		)

		cookie, _ := r.Cookie(core.SessionCookieName)
		if cookie == nil {
			// Нет сессии в печеньке, пробуем получить ее из JWT
			token := r.Header.Get(core.JWTHeaderName)
			if token == "" {
				next.ServeHTTP(w, r)

				return
			}

			user, err = domain.obj.AuthDomain().GetUserByJWT(r.Context(), token)
		} else {
			user, err = domain.obj.AuthDomain().GetUserBySession(r.Context(), cookie.Value)
		}

		// Нет данных или проблема, игнорируем
		if err != nil {
			logger.DebugError(r.Context(), err)
			next.ServeHTTP(w, r)

			return
		}

		// Устанавливаем данные пользователя в контекст
		*r = *r.WithContext(domain.obj.AuthDomain().CtxWithUser(r.Context(), user))

		next.ServeHTTP(w, r)
	})
}

func (_ *Domain) mainSeparateHandler(main, other http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			main.ServeHTTP(w, r)
		} else {
			other.ServeHTTP(w, r)
		}
	})
}

// MethodsSplitter - разделяет поток обработки в зависимости от метода
func (_ *Domain) MethodsSplitter(handlers map[string]http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler, found := handlers[r.Method]
		if !found {
			w.WriteHeader(http.StatusMethodNotAllowed)

			return
		}

		handler.ServeHTTP(w, r)
	})
}

func (domain *Domain) NoContentMock() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		domain.WriteNoContent(w)
	})
}

func (domain *Domain) panicDefenderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			p := recover()
			if p != nil {
				logger.Warning(r.Context(), "обнаружена паника", p)
				panicErr := errs.FromPanic(p)
				logger.Info(r.Context(), fmt.Sprintf("%v", errs.GetTrace(panicErr))) // TODO: сделать нормальный вывод
				domain.WriteJSON(r.Context(), w, http.StatusInternalServerError, panicErr)
			}
		}()
		if next != nil {
			next.ServeHTTP(w, r)
		}
	})
}

func serverRequiredMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		*r = *r.WithContext(
			ctxench.WithOriginPath(
				ctxench.WithRequestID(r.Context()),
				r.URL.Path+"?"+r.URL.RawQuery,
			),
		)

		w.Header().Set("X-SomeCome-SID", ctxench.GetServerID(r.Context()))
		w.Header().Set("X-SomeCome-CID", ctxench.GetConnectionID(r.Context()))
		w.Header().Set("X-SomeCome-RID", ctxench.GetRequestID(r.Context()))

		if next != nil {
			next.ServeHTTP(w, r)
		}
	})
}

const (
	// Хедер управления кешом.
	cacheControlHeader = "Cache-Control"
	// Не использовать кеш и отправить запрос на сервер.
	cacheControlNoCache = "no-cache"
	// Не сохранять кеш.
	cacheControlNoStore = "no-store"
	// Необходимость проверить статус кеша.
	cacheControlMustRevalidate = "must-revalidate"
	// Публичный (общий) кеш (может быть сохранен для всех пользователей браузера).
	cacheControlPublic = "public"
	// Приватный кеш (только для конкретного пользователя).
	cacheControlPrivate = "private"
	// Максимальный срок жизни кеша, это формат, а не значение!
	cacheControlMaxAgeFormat = "max-age=%d"
)

func CacheControlMiddleware(next http.Handler, liveTime time.Duration, isPrivate bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Удаляем другие значения хедера если они есть
		w.Header().Del(cacheControlHeader)

		switch {
		case liveTime == 0:
			// Кеш отключен/запрещен
			w.Header().Add(cacheControlHeader, cacheControlNoCache)
			w.Header().Add(cacheControlHeader, cacheControlNoStore)
			w.Header().Add(cacheControlHeader, cacheControlMustRevalidate)
		default:
			// Приватный или публичный кеш
			if isPrivate {
				w.Header().Add(cacheControlHeader, cacheControlPrivate)
			} else {
				w.Header().Add(cacheControlHeader, cacheControlPublic)
			}

			w.Header().Add(cacheControlHeader,
				fmt.Sprintf(cacheControlMaxAgeFormat, int(liveTime.Seconds())),
			)
		}

		if next != nil {
			next.ServeHTTP(w, r)
		}
	})
}
