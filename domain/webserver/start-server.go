package webserver

import (
	"context"
	"errors"
	"net"
	"net/http"
	"somecome/pkg/ctxench"
	"somecome/pkg/logger"
	"time"
)

func (domain *Domain) Start(ctx context.Context) (<-chan struct{}, error) {
	var err error

	mux := http.NewServeMux()

	mux.Handle("/", domain.rootPathHandler(ctx))
	mux.Handle("/favicon.ico", domain.NoContentMock())

	// Парсинг шаблонов
	err = domain.parseEmbeded(ctx)
	if err != nil {
		return nil, err
	}

	// Регистрация веб хандлеров модулей (new)
	err = domain.modulesFillMuxNew(ctx, mux)
	if err != nil {
		return nil, err
	}

	// Регистрация меню модулей (new)
	err = domain.modulesFillMenuNew(ctx)
	if err != nil {
		return nil, err
	}

	coreHandler := domain.sessionHandler(mux)

	return domain.startServer(ctx, domain.addr, coreHandler), nil
}

// startServer - инициализирует и запускает веб сервер
func (domain *Domain) startServer(rootCtx context.Context, addr string, coreHandler http.Handler) <-chan struct{} {
	closeChan := make(chan struct{})
	ctx := ctxench.WithDetach(rootCtx)

	server := http.Server{
		Addr:         addr,
		Handler:      serverRequiredMiddleware(domain.panicDefenderMiddleware(coreHandler)),
		ErrorLog:     logger.StdErrorLogger(ctx),
		BaseContext:  func(l net.Listener) context.Context { return ctxench.WithServerID(ctx) },
		ConnContext:  func(ctx context.Context, c net.Conn) context.Context { return ctxench.WithConnectionID(ctx) },
		ReadTimeout:  time.Second * 30,
		WriteTimeout: time.Second * 30,
	}

	go func() {
		defer close(closeChan)

		logger.Info(ctx, "Запущен веб сервер")
		defer logger.Info(ctx, "Веб сервер остановлен")

		err := server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error(ctx, err)
		}

	}()

	go func() {
		<-rootCtx.Done()
		logger.Info(ctx, "Остановка веб сервера")

		shutdownCtx, cancel := context.WithTimeout(ctx, time.Second*10)
		defer cancel()

		logger.IfErr(ctx, server.Shutdown(shutdownCtx))
	}()

	return closeChan
}
