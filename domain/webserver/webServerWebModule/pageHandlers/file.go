package pageHandlers

import (
	"net/http"
	"somecome/superobject"
)

func (o *Object) fileGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		download := r.URL.Query().Get("download") == "true"
		includeBody := r.Method != http.MethodOptions

		o.fileToWeb(wsd, r.Context(), w, token, download, includeBody)
	})
}
