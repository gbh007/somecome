package pageHandlers

import (
	"net/http"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/ctxench"
	"somecome/superobject"
)

// MenuItem - элемент полного меню
type MenuItem struct {
	// Ссылка на элемент
	Link string
	// Название элемента
	Name string
}

// MenuCategory - категория меню
type MenuCategory struct {
	// Название категории
	Name string
	// Пункты меню в категории
	Items []MenuItem
}

// MenuPageTD - данные для страницы с полным меню
type MenuPageTD struct {
	// Данные о категориях
	Categories []MenuCategory
}

func (o *Object) menuGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MenuPageTemplateName)
		data := MenuPageTD{}

		userData, _ := o.superObject.AuthDomain().GetUserFromContext(ctx)

		fullMenu := wsd.FilterFullMenu(
			ctx,
			webServerDTO.State{
				Authorized: userData != nil,
				DebugMode:  ctxench.GetDebugMode(ctx),
			},
		)

		for _, category := range fullMenu {
			items := []MenuItem{}

			for _, item := range category.Items {
				items = append(items, MenuItem{
					Link: item.Link,
					Name: item.Name,
				})
			}

			data.Categories = append(data.Categories, MenuCategory{
				Name:  category.Name,
				Items: items,
			})
		}

		tdata.SetData(data)

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}
