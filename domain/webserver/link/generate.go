package link

import (
	"net/url"
	"strconv"
)

// PageLinkGetter - тип для функций генераций ссылок на страницы
type PageLinkGetter func(pageNumber int64) string

// GetFileLink - генерирует ссылку на файл
func GetFileLink(fileToken string, download bool) string {
	link := url.URL{
		Path: PathToFile,
	}

	args := url.Values{
		"token": []string{fileToken},
	}

	if download {
		args.Set("download", "true")
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetAvatarLink - генерирует ссылку на аватар пользователя
func GetAvatarLink(userID int64) string {
	link := url.URL{
		Path: PathToAvatar,
	}

	args := url.Values{
		"id": []string{strconv.FormatInt(userID, 10)},
	}

	link.RawQuery = args.Encode()

	return link.String()
}

// GetReactionLink - генерирует ссылку на реакцию
func GetReactionLink(reactionID int64) string {
	link := url.URL{
		Path: PathToReaction,
	}

	args := url.Values{
		"id": []string{strconv.FormatInt(reactionID, 10)},
	}

	link.RawQuery = args.Encode()

	return link.String()
}
