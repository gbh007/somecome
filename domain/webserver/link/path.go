package link

// Ендпоинты веб сервера
const (
	PathToFile     = "/file"
	PathToAvatar   = "/avatar"
	PathToReaction = "/reaction"
	PathToLogout   = "/pages/auth/logout"
	PathToLogin    = "/pages/auth/login"
)
