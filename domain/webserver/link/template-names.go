package link

// PageTemplateName - тип для кода страницы рендеринга шаблона
type PageTemplateName struct {
	// Название шаблона
	TemplateName string
	// Название страницы
	PageName string
}

// Коды страниц для рендеринга шаблонов
var (
	MainPageTemplateName     = PageTemplateName{TemplateName: StdTemplateName, PageName: "core:page:main"}
	EmptyPageTemplateName    = PageTemplateName{TemplateName: StdTemplateName, PageName: "core:page:empty"}
	NotFoundPageTemplateName = PageTemplateName{TemplateName: StdTemplateName, PageName: "core:page:not-found"}
)

const (
	StdTemplateName = "page"
)
