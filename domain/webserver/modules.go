package webserver

import (
	"context"
	"html/template"
	"net/http"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/pkg/logger"
	"somecome/superobject"
)

// ModuleFillMuxNew - интерфейс модуля с регистрацией хандлеров
type ModuleFillMuxNew interface {
	// FillMux - регистрирует веб хандлеры
	FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux)
}

// ModuleFillMenuNew - интерфейс модуля с заполнением меню
type ModuleFillMenuNew interface {
	// FillMenu - заполняет меню
	FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error
}

// ModuleTemplater - интерфейс модуля с шаблонами для рендеринга
type ModuleTemplater interface {
	// FillTemplates - наполняет шаблоны для рендеринга страниц
	FillTemplates(ctx context.Context, t *template.Template) error
}

// RegisterModule - регистрирует веб модуль
func (domain *Domain) RegisterModule(ctx context.Context, module webServerDTO.Module) {
	domain.modules = append(
		domain.modules,
		module,
	)
}

// ActiveModules - информация об активных модулях
func (domain *Domain) ActiveModules(ctx context.Context) []webServerDTO.ModuleInfo {
	result := make([]webServerDTO.ModuleInfo, 0, len(domain.modules))

	for _, module := range domain.modules {
		info := webServerDTO.ModuleInfo{
			Name:       module.Name(),
			DomainName: module.DomainName(),
		}

		_, info.HasMenu = module.(ModuleFillMenuNew)
		_, info.HasHandlers = module.(ModuleFillMuxNew)
		_, info.HasTemplate = module.(ModuleTemplater)

		result = append(result, info)
	}

	return result
}

// HasModule - проверяет наличие веб модуля
func (domain *Domain) HasModule(ctx context.Context, name string) bool {
	for _, module := range domain.modules {
		if module.Name() == name {
			return true
		}
	}

	return false
}

// modulesFillMenuNew - заполняет меню из данных модулей
func (domain *Domain) modulesFillMenuNew(ctx context.Context) error {
	for _, module := range domain.modules {
		filler, ok := module.(ModuleFillMenuNew)
		if !ok {
			logger.Debug(ctx, "module:", module.Name(), " - no fill menu new")

			continue
		}

		err := filler.FillMenu(ctx, domain)
		if err != nil {
			return err
		}
	}

	return nil
}

// modulesFillMuxNew - регистрирует веб хандлеры модулей
func (domain *Domain) modulesFillMuxNew(ctx context.Context, mux *http.ServeMux) (err error) {
	defer func() {
		panicData := recover()
		if panicData != nil {
			err = errs.FromPanic(panicData)
		}
	}()

	for _, module := range domain.modules {
		filler, ok := module.(ModuleFillMuxNew)
		if !ok {
			logger.Debug(ctx, "module:", module.Name(), " - no web handlers new")

			continue
		}

		filler.FillMux(ctx, domain, mux)
	}

	return
}
