package webServerDTO

// MenuItemInternal - элемент меню
type MenuItemInternal struct {
	// Ссылка на элемент
	Link string
	// Название элемента
	Name string
	// Полное название для главного меню
	NameInMainMenu string

	// Зависимость от авторизации
	Login LoginParams
	// Зависимость от отладки
	Debug DebugParams
	// Отображать в главном меню
	ShowInMainMenu bool

	// Зависимость от прав доступа
	Permissions PermissionCheck
}

// MenuCategoryInternal - категория меню
type MenuCategoryInternal struct {
	// Название категории
	Name string
	// Пункты меню в категории
	Items []MenuItemInternal

	// Зависимость от авторизации
	Login LoginParams
	// Зависимость от отладки
	Debug DebugParams

	// Зависимость от прав доступа
	Permissions PermissionCheck

	// Приоритет, выше - отображается раньше
	Priority byte
}

// PermissionCheck - данные для проверки доступа
type PermissionCheck struct {
	// Проверка включена
	Enable bool
	// Код доступа
	Code string
	// Уровень доступа
	Level int16
}
