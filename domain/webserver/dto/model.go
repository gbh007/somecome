package webServerDTO

/*

// InitModule - инициализирует модуль ЧЕГО
func InitModule() *Module { return &Module{} }

// Module - модуль ЧЕГО
type Module struct{}

// DomainName - название домена модуля
func (_ *Module) DomainName() string { return "" }

// Name - название модуля
func (_ *Module) Name() string { return "" }

// FillMux - регистрирует веб хандлеры
func (_ *Module) FillMux(ctx context.Context, wsd core.WebServerDomain, mux *http.ServeMux) {}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd core.WebServerDomain) error { return nil }

// FillTemplates - наполняет шаблоны для рендеринга страниц
func (_ *Module) FillTemplates(ctx context.Context, t *template.Template) error { return nil }

*/

// Module - базовый интерфейс модуля
type Module interface {
	// DomainName - название домена модуля
	DomainName() string
	// Name - название модуля
	Name() string
}

// ModuleInfo - информация о веб модуле
type ModuleInfo struct {
	// Название домена
	DomainName string
	// Название модуля
	Name string
	// Имеет ли модуль веб обработчики
	HasHandlers bool
	// Имеет ли модуль меню
	HasMenu bool
	// Имеет ли модуль шаблоны
	HasTemplate bool
}
