package webServerDTO

// TemplateData - интерфейс с данными для рендеринга шаблона
type TemplateData interface {
	// Template - название шаблона для рендеринга
	Template() string
	// WithError - добавляет в ответ данные ошибки
	WithError(err error)
	// SetData - устанавливает в ответ данные для рендеринга
	SetData(data interface{})
}

// Page - данные для рендеринга номеров страниц (пагинации)
type Page struct {
	// Номер страницы
	Number int64
	// Признак того что это разделитель
	IsSeparator bool
	// Ссылка на страницу
	Link string
	// Это текущая страница
	IsCurrent bool
}
