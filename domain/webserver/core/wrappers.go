package core

import (
	"net/http"
	"somecome/superobject"
)

func PostWithNoOptionWrapper(wsd superobject.WebServerDomain, next http.Handler) http.Handler {
	return wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodPost:    next,
	})
}

func GetWithNoOptionWrapper(wsd superobject.WebServerDomain, next http.Handler) http.Handler {
	return wsd.MethodsSplitter(map[string]http.Handler{
		http.MethodOptions: wsd.NoContentMock(),
		http.MethodGet:     next,
	})
}
