package core

import "errors"

var (
	// MultipleFilesWithOneTokenErr - более 1 файла на токен
	MultipleFilesWithOneTokenErr = errors.New("multiple files with one token")
	// NoFileToUploadErr - не предоставлены файлы для загрузки
	NoFileToUploadErr = errors.New("no file to upload")
	// FileToLargeErr - слишком большой размер файла
	FileToLargeErr = errors.New("file to large")
)
