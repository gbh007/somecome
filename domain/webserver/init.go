package webserver

import (
	"context"
	"embed"
	"fmt"
	"html/template"
	"somecome/config"
	webServerDTO "somecome/domain/webserver/dto"
	"somecome/domain/webserver/webServerWebModule"
	"somecome/superobject"

	"github.com/jmoiron/sqlx"
)

//go:embed template/*
var templateDir embed.FS

//go:embed static/*
var staticDir embed.FS

// embedTemplate - данные для шаблонов
type embedTemplate struct {
	data *template.Template
	err  error
}

// Domain - домен для веб сервера
type Domain struct {
	// Меню для рендеринга пользователям
	menu []webServerDTO.MenuCategoryInternal

	// Путь до файлов статики
	debugStaticDirPath string
	// Путь до шаблонов основной части
	debugTemplateDirPath string

	// Данные для встроенного шаблонизатора
	embedTemplate *embedTemplate

	// Подключенные веб модули
	modules []webServerDTO.Module

	// Супер объект
	obj superobject.SuperObject
	// Веб модуль
	module *webServerWebModule.Module

	// Адрес веб сервера
	addr string
}

// Name - название домена
func (_ *Domain) Name() string { return "webserver" }

// InnerInit - инициализирует домен
func (wsd *Domain) InnerInit(ctx context.Context, obj superobject.SuperObject) error {
	wsd.obj = obj
	wsd.module = webServerWebModule.InitModule(obj, wsd.Name())

	return nil
}

// ExternalInit - подготавливает домен
func (domain *Domain) ExternalInit(ctx context.Context) error {
	domain.obj.WebServerDomain().RegisterModule(ctx, domain.module)

	return nil
}

// Configure - конфигурирует домен
func (domain *Domain) Configure(ctx context.Context, db *sqlx.DB, cfg *config.Config) error {
	domain.debugStaticDirPath = "domain/webserver/static"
	domain.debugTemplateDirPath = "domain/webserver/template"
	domain.embedTemplate = new(embedTemplate)
	domain.addr = fmt.Sprintf(":%d", cfg.System.WebServerPort)

	return nil
}
