window.addEventListener("load", function () {
  function appAlert(text) {
    window.dispatchEvent(
      new CustomEvent("app-runtime-error", {
        detail: { text: text },
      })
    );
  }

  function refreshDependencies(text) {
    window.dispatchEvent(new Event("app-module-reaction-refresh"));
    window.dispatchEvent(new Event("app-module-txtp-refresh"));
    window.dispatchEvent(new Event("app-pretty-time-refresh"));
    window.dispatchEvent(new Event("app-module-content-refresh-tags"));
  }

  function renderPages(node) {
    node
      .querySelectorAll("div.pagination-page[tp=page]")
      .forEach((pageNode) => {
        pageNode.onclick = () => {
          node.setAttribute("page", pageNode.getAttribute("page"));
          node.setAttribute("unprocessed", "");
          window.dispatchEvent(new Event("app-module-comment-refresh"));
        };
      });
  }

  function refreshOneNode(node) {
    const page = node.getAttribute("page");
    const contentKey = node.getAttribute("content-key");

    // TODO: заменить на нормальную ссылку
    fetch(
      `/pages/comment/list?content-key=${encodeURIComponent(
        contentKey
      )}&page=${encodeURIComponent(page)}&redirect=${encodeURIComponent(
        window.location.href
      )}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        response.text().then((text) => {
          if (response.ok) {
            node.innerHTML = text;
            renderPages(node);
            node.removeAttribute("unprocessed");
            refreshDependencies();
          } else {
            appAlert(text);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function refreshAll() {
    document
      .querySelectorAll("div.comment-root[unprocessed]")
      .forEach((node) => {
        refreshOneNode(node);
      });
  }

  refreshAll();

  window.addEventListener("app-module-comment-refresh", refreshAll);
});
