function renderDatetime(value, onchange = null) {
  let dtNode = document.createElement("input");
  dtNode.className = "std";
  dtNode.type = "datetime-local";

  let parsed = new Date(value);
  parsed.setHours(parsed.getHours() - new Date().getTimezoneOffset() / 60);
  dtNode.value = parsed.toISOString().slice(0, 16);

  dtNode.onchange = () => {
    if (!onchange) return;
    onchange(new Date(dtNode.value).toJSON());
  };

  return dtNode;
}
