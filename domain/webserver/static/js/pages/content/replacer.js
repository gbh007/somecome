window.addEventListener("load", function () {
  function appAlert(text) {
    window.dispatchEvent(
      new CustomEvent("app-runtime-error", {
        detail: { text: text },
      })
    );
  }

  function refreshOneNode(node) {
    const contentKey = node.getAttribute("content-key");

    // TODO: заменить на нормальную ссылку
    fetch(
      `/pages/content/tags?content-key=${encodeURIComponent(
        contentKey
      )}&redirect=${encodeURIComponent(window.location.href)}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        response.text().then((text) => {
          if (response.ok) {
            node.innerHTML = text;
            node.removeAttribute("unprocessed");
          } else {
            appAlert(text);
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function refreshAll() {
    document
      .querySelectorAll("div.content-tag-list[unprocessed]")
      .forEach((node) => {
        refreshOneNode(node);
      });
  }

  refreshAll();

  window.addEventListener("app-module-content-refresh-tags", refreshAll);
});
