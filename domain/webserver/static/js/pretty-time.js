window.addEventListener("load", function () {
  function updateNode(node) {
    const dt = new Date(node.getAttribute("value"));
    node.innerText = dt.toLocaleString();
    node.removeAttribute("unprocessed");
  }

  function refreshAll() {
    document
      .querySelectorAll("span.pretty-time[unprocessed]")
      .forEach((node) => {
        updateNode(node);
      });
  }

  refreshAll();

  window.addEventListener("app-pretty-time-refresh", refreshAll);
});
