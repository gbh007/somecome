package plain

import (
	"bufio"
	"io"
	txtpDTO "somecome/domain/txtp/dto"
)

func Parse(r io.Reader) []*txtpDTO.TextUnit {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	units := make([]*txtpDTO.TextUnit, 0)
	for scanner.Scan() {
		units = append(units, &txtpDTO.TextUnit{
			Value:        scanner.Text(),
			Type:         txtpDTO.PlainTextUT,
			HasLineBreak: true,
		})
	}

	return units
}
