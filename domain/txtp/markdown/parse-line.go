package markdown

import (
	txtpDTO "somecome/domain/txtp/dto"
)

func rowLineParse(row []rune, extendMode bool) (string, []*txtpDTO.TextUnit) {
	var (
		listUnits []*txtpDTO.TextUnit
		value     string
		outerBuff []rune
		lexBuff   []rune
		prev      rune
		skipFor   int
	)

	for index, r := range row {

		isL := isLexeme(prev, lexBuff, r)
		inside := len(lexBuff) > 0

		prev = r

		if index < skipFor {
			continue
		}

		if isL {
			lexBuff = append(lexBuff, r)

			continue
		}

		if !isL && !inside {
			outerBuff = append(outerBuff, r)

			continue
		}

		endIndex := indexLexeme(row, index, lexBuff)

		// Это не лексема, или она не корректна
		if endIndex == -1 {
			outerBuff = append(outerBuff, lexBuff...)
			outerBuff = append(outerBuff, r)
			lexBuff = []rune{}

			continue
		}

		innerBuff := row[index:endIndex]
		skipFor = endIndex + len(lexBuff)

		if len(outerBuff) > 0 {
			listUnits = append(listUnits, &txtpDTO.TextUnit{
				Value: string(outerBuff),
				Type:  txtpDTO.PlainTextUT,
			})
		}

		subValue, subUnits := rowLineParse(innerBuff, extendMode)

		listUnits = append(listUnits, &txtpDTO.TextUnit{
			Value:    subValue,
			Type:     lexemeType(lexBuff, extendMode),
			Children: subUnits,
		})

		lexBuff = []rune{}
		outerBuff = []rune{}
	}

	if len(listUnits) > 0 && len(outerBuff) > 0 {
		listUnits = append(listUnits, &txtpDTO.TextUnit{
			Value: string(outerBuff),
			Type:  txtpDTO.PlainTextUT,
		})
	}

	if len(listUnits) == 0 {
		value = string(row)
	}

	return value, listUnits
}

func isLexeme(prev rune, full []rune, r rune) bool {
	if prev == '\\' {
		return false
	}

	switch r {
	case '~':
		if len(full) == 0 {
			return true
		}

		if r != prev {
			return false
		}

		return len(full) < 2

	case '*', '_':
		if len(full) == 0 {
			return true
		}

		if r != prev {
			return false
		}

		return len(full) < 3

	case '`':
		return len(full) == 0
	}

	return false
}

func lexemeType(lexeme []rune, extendMode bool) txtpDTO.UnitType {
	lex := string(lexeme)

	switch lex {
	case "***", "___":
		return txtpDTO.BoldCursiveTextUT
	case "~~":
		return txtpDTO.StrikeTextUT
	case "**":
		return txtpDTO.BoldTextUT
	case "__":

		if extendMode {
			return txtpDTO.UnderlineTextUT
		}

		return txtpDTO.BoldTextUT
	case "*", "_":
		return txtpDTO.CursiveTextUT
	case "`":
		return txtpDTO.CodeUT
	default:
		return txtpDTO.PlainTextUT
	}
}

func indexLexeme(row []rune, from int, lexeme []rune) int {
	l := len(row)
	if from >= l || len(lexeme) == 0 {
		return -1
	}

	position := from

	for position < l {

		// Экранирование
		if row[position] == '\\' {
			position += 2
		}

		for shift, lexemeRune := range lexeme {
			comparePosition := position + shift

			if comparePosition >= l {
				break
			}

			if row[comparePosition] != lexemeRune {
				break
			}

			// Все проверки пройдены, вхождение найдено
			if shift == len(lexeme)-1 {
				return position
			}

		}

		position++
	}

	return -1
}
