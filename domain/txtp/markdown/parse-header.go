package markdown

import txtpDTO "somecome/domain/txtp/dto"

func headerBlockParse(row []rune, extendMode bool) (*txtpDTO.TextUnit, bool) {
	size := 0

	for index, c := range row {
		if index > 5 || size > 5 {
			break
		}

		if c != '#' {
			break
		}

		size++
	}

	if size == 0 {
		return nil, false
	}

	value, subs := rowLineParse(row[size:], extendMode)

	return &txtpDTO.TextUnit{
		Value:        value,
		Type:         txtpDTO.HeaderUT,
		HeaderNumber: int8(size),
		Children:     subs,
	}, true
}
