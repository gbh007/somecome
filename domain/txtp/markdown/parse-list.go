package markdown

import txtpDTO "somecome/domain/txtp/dto"

func markedDetect(row []rune, extendMode bool) (string, []*txtpDTO.TextUnit, bool) {
	if len(row) < 2 {
		return "", nil, false
	}

	if row[0] != '-' || row[1] != ' ' {
		return "", nil, false
	}

	value, subs := rowLineParse(row[2:], extendMode)

	return value, subs, true
}

func numericDetect(row []rune, extendMode bool) (string, []*txtpDTO.TextUnit, bool) {
	_, size, found := searchNumber(row)
	if !found {
		return "", nil, false
	}

	if len(row) < size+2 {
		return "", nil, false
	}

	if row[size] != '.' || row[size+1] != ' ' {
		return "", nil, false
	}

	value, subs := rowLineParse(row[size+2:], extendMode)

	return value, subs, true
}

func listBlockParse(row []rune, extendMode bool) (*txtpDTO.TextUnit, bool) {
	spaceCount := spaceDetect(row)
	if spaceCount == len(row) || len(row) < 1 {
		return nil, false
	}

	unit := &txtpDTO.TextUnit{
		SpaceBefore:  spaceCount,
		HasLineBreak: hasTrueBR(row),
	}

	found := false

	if v, subs, ok := markedDetect(row[spaceCount:], extendMode); ok {
		unit.Value = v
		unit.Type = txtpDTO.MarkListElementUT
		unit.Children = subs
		found = true
	}

	if v, subs, ok := numericDetect(row[spaceCount:], extendMode); ok {
		unit.Value = v
		unit.Type = txtpDTO.NumericListElementUT
		unit.Children = subs
		found = true
	}

	return unit, found
}
