package markdown

import "strconv"

func spaceDetect(row []rune) int {
	count := 0

search:
	for _, c := range row {
		switch c {
		case ' ':
			count++
		case '\t':
			count += 4
		default:
			break search
		}

	}

	return count
}

func hasTrueBR(row []rune) bool {
	l := len(row)
	if l == 0 {
		return true
	}

	if l < 2 {
		return false
	}

	if row[l-1] == ' ' && row[l-2] == ' ' {
		return true
	}

	return false
}

func searchNumber(row []rune) (number int64, size int, found bool) {
	numberRaw := make([]rune, 0)

	for _, c := range row {
		// Символ вне диапазона цифр
		if c > '9' || c < '0' {
			break
		}

		numberRaw = append(numberRaw, c)
		found = true
		size++
	}

	var err error

	number, err = strconv.ParseInt(string(numberRaw), 10, 64)
	if err != nil {
		found = false
	}

	return
}
