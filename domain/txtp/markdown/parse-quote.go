package markdown

import txtpDTO "somecome/domain/txtp/dto"

func quoteBlockParse(row []rune, extendMode bool) (*txtpDTO.TextUnit, bool) {
	spaceBefore := spaceDetect(row)
	if len(row) <= spaceBefore+1 {
		return nil, false
	}

	if row[spaceBefore] != '>' {
		return nil, false
	}

	value, subs := rowLineParse(row[spaceBefore+1:], extendMode)

	return &txtpDTO.TextUnit{
		Value:        value,
		Type:         txtpDTO.QuoteUT,
		Children:     subs,
		SpaceBefore:  spaceBefore,
		HasLineBreak: true,
	}, true
}
