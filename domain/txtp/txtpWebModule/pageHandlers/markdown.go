package pageHandlers

import (
	"net/http"
	txtpDTO "somecome/domain/txtp/dto"
	"somecome/superobject"
	"strings"
)

// TextRenderUnit - данные для рендеринга текстов
type TextRenderUnit struct {
	// Значение элемента, если есть
	Value string
	// Тип элемента
	Type string
	// Дочерние элементы
	Children []TextRenderUnit
	// Размер заголовка, только для типа заголовка
	HeaderNumber int8
	// Элемент имеет истинный перенос по спецификации markdown
	HasTrueBr bool
}

func (o *Object) markdownPostHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, MarkdownTemplateName)

		dialect := r.Header.Get("X-Dialect")

		defer r.Body.Close()
		units, err := o.superObject.TextProcessorDomain().Parse(ctx, r.Body, txtpDTO.Dialect(dialect))
		if err != nil {
			wsd.WritePlain(ctx, w, http.StatusBadRequest, err.Error())

			return
		}

		tdata.SetData(convertToRender(units))
		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func (o *Object) testMDGetHandler(wsd superobject.WebServerDomain) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		tdata := wsd.TemplateData(ctx, TestMDPageTemplateName)

		tdata.SetData(map[string]interface{}{
			"RequestLink": PathToTesting,
		})

		wsd.WriteTemplate(ctx, w, http.StatusOK, tdata)
	})
}

func convertToRender(rawUnits []*txtpDTO.TextUnit) []TextRenderUnit {
	units := []TextRenderUnit{}

	for _, rawUnit := range rawUnits {
		unit := TextRenderUnit{
			Value:        rawUnit.Value,
			Type:         string(rawUnit.Type),
			HeaderNumber: rawUnit.HeaderNumber,
			HasTrueBr:    rawUnit.HasLineBreak,
		}

		switch rawUnit.Type {
		case txtpDTO.BlockCodeUT:
			values := []string{}

			for _, rawSubUnit := range rawUnit.Children {
				values = append(values, rawSubUnit.Value)
			}

			unit.Value = strings.Join(values, "\n")

		default:
			unit.Children = convertToRender(rawUnit.Children)
		}

		units = append(units, unit)
	}

	return units
}
