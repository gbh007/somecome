package txtpWebModule

import (
	"context"
	"net/http"

	// "somecome/domain/txtp/txtpWebModule/apiHandlers"
	"somecome/domain/txtp/txtpWebModule/pageHandlers"

	webServerDTO "somecome/domain/webserver/dto"
	"somecome/pkg/errs"
	"somecome/superobject"
)

var (
	// Ошибка модуля обработки текстов
	ModuleError = errs.NewGroup("text-processor module")
)

// InitModule - инициализирует модуль обработки текстов
func InitModule(obj superobject.SuperObject, domainName string) *Module {
	return &Module{
		pageHandler: pageHandlers.Init(obj),
		// apiHandler:  apiHandlers.Init(obj),
		domainName: domainName,
	}
}

// Module - модуль обработки текстов
type Module struct {
	pageHandler *pageHandlers.Object
	// apiHandler  *apiHandlers.Object
	domainName string
}

// Name - название модуля
func (_ *Module) Name() string { return "txtp" }

// DomainName - название домена модуля
func (m *Module) DomainName() string { return m.domainName }

// FillMux - регистрирует веб хандлеры
func (m *Module) FillMux(ctx context.Context, wsd superobject.WebServerDomain, mux *http.ServeMux) {
	m.pageHandler.FillMux(wsd, mux)
	// m.apiHandler.FillMux(wsd, mux)
}

// FillMenu - заполняет меню
func (_ *Module) FillMenu(ctx context.Context, wsd superobject.WebServerDomain) error {
	err := wsd.AddCategory(ctx, category)
	if err != nil {
		return errs.WrapError(ctx, ModuleError, err)
	}

	return nil
}

var category = webServerDTO.MenuCategoryInternal{
	Name: "Текстовый процессор",
	Items: []webServerDTO.MenuItemInternal{
		{
			Link: pageHandlers.PathToTesting,
			Name: "Тест",
		},
	},
	Debug: webServerDTO.DebugEnableOnly,
}
